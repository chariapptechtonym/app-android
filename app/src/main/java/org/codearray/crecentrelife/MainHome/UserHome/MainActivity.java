package org.codearray.crecentrelife.MainHome.UserHome;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import org.codearray.crecentrelife.MainHome.MyNetworks.NetworkMainActivity;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.UserBenifitsActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Family.FamilyActivity;
import org.codearray.crecentrelife.MainHome.UserHome.HomeFrags.MasjidFrag;
import org.codearray.crecentrelife.MainHome.UserHome.HomeFrags.NamazTimeFrag;
import org.codearray.crecentrelife.MainHome.UserHome.HomeFrags.QiblaDirectionFraf;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.FieldOfficerProfile;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.SponserProfile;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.UserProfile;
import org.codearray.crecentrelife.MainHome.UserHome.Projects.MainProjectsActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.MainHome.ViewPager.CustomViewPager;
import org.codearray.crecentrelife.R;


import org.json.JSONArray;
import org.json.JSONObject;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.afollestad.materialdialogs.MaterialDialog;

import android.provider.Settings.Secure;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    //sources
    ImageView _familyBtn;
    ImageView _networkBtn;
    ImageView _benifitsBtn;
    ImageView _projectBtn;
    ImageView _profileBtn;
    AlertDialog dialog;
    ImageView websitelink;
    EditText etFName,etLName,etPhoneNumber,etEmail,etPassword;
    TextView ismaicDate;
    TextView myDate;
    Button btnLocation,btnDirections,btnPrayers,btnForgotPassword,btnSignup;
    String userrole;
    LoginSession loginSession;
    SharedPreferences prefs;
    String android_id;
    CustomViewPager mViewPager;
    boolean isFirstTime;
    TabLayout tabLayout;
    private void initViews() {

        isFirstTime = true;
       android_id = Secure.getString(this.getContentResolver(),
                Secure.ANDROID_ID);

//       SilentRegister();
        _familyBtn = (ImageView) findViewById(R.id.family_btn);
        _networkBtn = (ImageView) findViewById(R.id.network_btn);
        _benifitsBtn = (ImageView) findViewById(R.id.benifits_btn);
        _projectBtn = (ImageView) findViewById(R.id.projects_btn);
        _profileBtn = (ImageView) findViewById(R.id.profile_btn);
        myDate = (TextView) findViewById(R.id.home_screen_date);
        websitelink = (ImageView) findViewById(R.id.toWebsitelink_id);
        ismaicDate = (TextView) findViewById(R.id.islamic_date_id);
        tabLayout = (TabLayout) findViewById(R.id.tabDots);
        btnLocation = (Button)findViewById(R.id.btnLocations);
        btnDirections = (Button)findViewById(R.id.Nearby);
        btnPrayers = (Button)findViewById(R.id.btnPrayers);

        new GetUserID().execute();

    }


    public class SamplePagerAdapter extends FragmentPagerAdapter {

        public SamplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            /** Show a Fragment based on the position of the current screen */

            if(position == 0){
                //locationSelected();
                return new MasjidFrag();
            }
            else if(position == 1){
                //prayersSelected();
                return new NamazTimeFrag();
            }
            else if(position == 2)
            {
                return new QiblaDirectionFraf();
            }
            else {
               return  new NamazTimeFrag();
            }


        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 3;
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //startup views
        initViews();


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        myDate.setText(formattedDate);

        mViewPager = (CustomViewPager) findViewById(R.id.pager);

        mViewPager.setAdapter(new SamplePagerAdapter(
                getSupportFragmentManager()));
        mViewPager.setEnabled(false);
        mViewPager.setPagingEnabled(false);



        //tabLayout.setupWithViewPager(mViewPager, false);
    //tabLayout.setSelectedTabIndicatorColor(R.color.zxing_transparent);
    //tabLayout.setSelectedTabIndicatorHeight(0);
        btnPrayers.setText("");
        btnLocation.setText("");
        btnDirections.setText("");
        btnPrayers.setOnClickListener(this);
        btnLocation.setOnClickListener(this);
        btnDirections.setOnClickListener(this);
        prayersSelected();
        _familyBtn.setOnClickListener(this);
        _networkBtn.setOnClickListener(this);
        _benifitsBtn.setOnClickListener(this);
        _projectBtn.setOnClickListener(this);
        _profileBtn.setOnClickListener(this);
        websitelink.setOnClickListener(this);


//        UmmalquraCalendar now = new UmmalquraCalendar();
//        String finalDate = now.get(UmmalquraCalendar.DAY_OF_MONTH)+" - "+now.get(UmmalquraCalendar.MONTH)+" - "+ now.get(UmmalquraCalendar.YEAR);
//
//        ismaicDate.setText(finalDate);
       // Toast.makeText(getApplicationContext() , "Height: = "+ String.valueOf(dpHeight)+"---- Widht: ="+ String.valueOf(dpWidth), Toast.LENGTH_LONG).show();





    }



    @Override
    public void onResume(){
        super.onResume();

       // mViewPager.setAdapter(new SamplePagerAdapter(
          //      getSupportFragmentManager()));
//        if(isFirstTime) {

        btnLocation.setSelected(false);
        btnPrayers.setSelected(true);
        btnDirections.setSelected(false);
        mViewPager.setCurrentItem(1);
        isFirstTime = false;
        prayersSelected();

    }
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.family_btn:
               // AppConsts.ShowToast(getApplicationContext() , "FamilyBtn");
                Intent startFam = new Intent(MainActivity.this, FamilyActivity.class);
                startActivity(startFam);
//                finish();

                break;
            case R.id.btnPrayers:
                btnLocation.setSelected(false);
                btnPrayers.setSelected(true);
                btnDirections.setSelected(false);
                prayersSelected();
                mViewPager.setCurrentItem(1);
                break;
            case R.id.btnLocations:
                btnLocation.setSelected(true);
                btnPrayers.setSelected(false);
                btnDirections.setSelected(false);
                locationSelected();
                mViewPager.setCurrentItem(0);
                break;
            case R.id.Nearby:
                btnLocation.setSelected(false);
                btnPrayers.setSelected(false);
                btnDirections.setSelected(true);
                directionSelected();
                mViewPager.setCurrentItem(2);
                break;
            case R.id.network_btn:
               // AppConsts.ShowToast(getApplicationContext() , "Networks");
                if(prefs.getString("user_ref_id","") != "") {
                    Intent startNet = new Intent(MainActivity.this, NetworkMainActivity.class);
                    startActivity(startNet);
//                    finish();
                }
                else
                {
                    showPopup();

                }
                break;

            case R.id.benifits_btn:
                if(prefs.getString("user_ref_id","") != "") {
                    // AppConsts.ShowToast(getApplicationContext() , "Benifits");
                    Intent startBen = new Intent(MainActivity.this, UserBenifitsActivity.class);
                    startActivity(startBen);
//                    finish();
                }
                else
                {
                showPopup();
                }
                break;

            case R.id.projects_btn:
                //AppConsts.ShowToast(getApplicationContext() , "Projects");
                if(prefs.getString("user_ref_id","") != "") {
                    Intent startPro = new Intent(MainActivity.this, MainProjectsActivity.class);
                    startActivity(startPro);
//                    finish();
                }
                else
                {
                    showLogin();
//                    showPopup();
                }
                break;

            case R.id.profile_btn:
                String check = prefs.getString("user_ref_id","");
                userrole = prefs.getString("role_name","");
                if(prefs.getString("user_ref_id","") != "") {
                    if (userrole != null && userrole.equalsIgnoreCase("Sponser")) {
                        Intent startprofile = new Intent(MainActivity.this, SponserProfile.class);
                        startActivity(startprofile);
                    } else if (userrole != null && userrole.equalsIgnoreCase("Officer")) {
                        // AppConsts.ShowToast(getApplicationContext() , "Projects");
                        Intent startprofile = new Intent(MainActivity.this, FieldOfficerProfile.class);
                        startActivity(startprofile);

//                        finish();
                    } else {
                        Intent startprofile = new Intent(MainActivity.this, UserProfile.class);
                        startActivity(startprofile);

//                        finish();
//                    showPopup();
                    }
                }
                else
                {
                    showLogin();
//                    showPopup();

                }
                break;

            case R.id.toWebsitelink_id:
                Uri uri = Uri.parse("http://crescentrelief.org.au/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

        }

    }
    private void directionSelected()
    {
        btnDirections.setBackgroundResource(R.drawable.directions_selected);
        btnPrayers.setBackgroundResource(R.drawable.prayers_unselected);
        btnLocation.setBackgroundResource(R.drawable.locations_unselected);
    }
    private void locationSelected()
    {
        btnDirections.setBackgroundResource(R.drawable.directions_unselected);
        btnPrayers.setBackgroundResource(R.drawable.prayers_unselected);
        btnLocation.setBackgroundResource(R.drawable.locations_selected);
    }
    private void prayersSelected()
    {
        btnDirections.setBackgroundResource(R.drawable.directions_unselected);
        btnPrayers.setBackgroundResource(R.drawable.prayers_selected);
        btnLocation.setBackgroundResource(R.drawable.locations_unselected);
    }
    private void showLogin()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_login_popup, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        Button btnSignup,btnForgotPassword;
        btnSignup = (Button)alertLayout.findViewById(R.id.btnSignup);
        btnForgotPassword = (Button)alertLayout.findViewById(R.id.btnForgotPassword);
        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
        etPassword = (EditText)alertLayout.findViewById(R.id.editPassword);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();
        final AlertDialog currentDialog =dialog;
                dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etPassword.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                new LoginTask().execute();
                }
//                else
//                {
//                    new UpdateInfoTask().execute();
//
//                }

            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSignup();
                Log.e("button","Signup pressed");

            }
        });
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForgotPassword();
               Log.e("button","forgot pressed");
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentDialog.dismiss();
            }
        });
    }
    private void showPopup()
    {
        showLogin();

//        LayoutInflater inflater = getLayoutInflater();
//        View alertLayout = inflater.inflate(R.layout.information_alert_dialog, null);
//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//
//        etFName = (EditText)alertLayout.findViewById(R.id.editFirstName);
//        etLName = (EditText)alertLayout.findViewById(R.id.editLastName);
//        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
//        etPhoneNumber = (EditText)alertLayout.findViewById(R.id.editPhoneNumber);
//        // this is set the view from XML inside AlertDialog
//        alert.setView(alertLayout);
//        // disallow cancel of AlertDialog on click of back button and outside touch
//        alert.setCancelable(false);
//        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
//        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
//        dialog = alert.create();
//
//        dialog.show();
//        btnSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.e("Success","Ok now");
//                if(etEmail.getText().length()==0 || etFName.getText().length() == 0 || etPhoneNumber.getText().length() == 0 || etLName.getText().length() == 0 )
//                {
//                    Log.e("Error","Pls fill the required fields");
//                }
//                else
//                {
//                    new UpdateInfoTask().execute();
//
//                }
//
//            }
//        });
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });

    }

    public void showSignup()
    {
                LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.information_alert_dialog, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        etFName = (EditText)alertLayout.findViewById(R.id.editFirstName);
        etLName = (EditText)alertLayout.findViewById(R.id.editLastName);
        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
        etPhoneNumber = (EditText)alertLayout.findViewById(R.id.editPhoneNumber);
        etPassword = (EditText)alertLayout.findViewById(R.id.editPassword);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();
        AlertDialog currentDialog =dialog;
        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etFName.getText().length() == 0 || etPhoneNumber.getText().length() == 0 || etLName.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new UpdateInfoTask().execute();

                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }
        });
    }



    private void SilentRegisterPrevious()
    {

// then you use

        if( prefs.getBoolean("isUserLoggedIn", false) == true)
        {
//            SilentLogin();
        }
        else
        {
            new SilentRegisterTask().execute();
        }
    }

    private void SilentLoginPrevious()
    {
        String requestUrl = "http://api.chariapp.com/api/public/users/accountlogin";
        new SilentLoginTask().execute();
    }
    public class GetUserID  extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String currentUserId = null;
            loginSession = new LoginSession(MainActivity.this);
            HashMap<String ,String> detail = loginSession.getUserDetails();
            currentUserId =detail.get(LoginSession.user_role);
            return currentUserId;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            userrole = s;
        }
    }

    public class SilentRegisterTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.DoSilentRegister(android_id,"");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                if(s.contains("forbidden"))
                {

                }
                else {
//                    SilentLogin();
                }

            }catch (Exception e)
            {
                Log.e("Error","Something went wrong");
            }





        }
    }

    public class UpdateInfoTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.registerNewUser("",android_id,etFName.getText().toString(),etLName.getText().toString(),etPhoneNumber.getText().toString(),"",etEmail.getText().toString(),"","","","","",etPassword.getText().toString(),"1");
//                responseString = HttpRequests.UpdateInfo(android_id,etFName.getText().toString(),etLName.getText().toString(),etEmail.getText().toString(),etPhoneNumber.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                JSONObject jsonObj = new JSONObject(s);
                String msg = jsonObj.getString("msg").toString();
                if(s.contains("forbidden"))
                {

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Success");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {

                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();

//                    JSONObject jsonObj = new JSONObject(s);
//
//                    JSONObject getData = jsonObj.getJSONObject("user");
//
////                    JSONArray getData = jsonObj.getJSONArray("user");
//                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
//
////                        JSONObject c = getData.getJSONObject(i);
//                        int user_id = (int) getData.get("user_id");
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("user_id",user_id+"");
//                        String user_ref_id = (String) getData.get("user_ref_id").toString();
//                        editor.putString("user_ref_id",user_ref_id);
//                        String user_first_name =(String)  getData.get("user_first_name").toString();
//                        editor.putString("user_first_name",user_first_name);
//                        String user_last_name =(String)  getData.get("user_last_name").toString();
//                        editor.putString("user_last_name",user_last_name);
//                        String user_email = (String) getData.get("user_email").toString();
//                        editor.putString("user_email",user_email);
//                        String user_mobile =(String)  getData.get("user_mobile").toString();
//                        editor.putString("user_mobile",user_mobile);
//                        String country =(String)  getData.get("user_add_cont").toString();
////                        editor.putString("user_add_cont",country);
//                        String state = (String) getData.get("user_add_state").toString();
////                        editor.putString("user_add_state",state);
//                        String city = (String) getData.get("user_add_city").toString();
////                        editor.putString("user_add_city",city);
//                        String street =(String)  getData.get("user_add_st").toString();
////                        editor.putString("user_add_st",street);
//                        int ou_id = (int) getData.get("ou_id");
//                        editor.putString("ou_id",ou_id+"");
//                        String user_role =(String)  getData.get("role_name").toString();
//                        editor.putString("role_name",user_role);
//                        editor.putBoolean("isUserLoggedIn",true);
//                        editor.commit();



                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                }

            }catch (Exception e)
            {
            Log.e("exception","exception occured");
            Toast.makeText(getApplicationContext(),e.getMessage().toString(),Toast.LENGTH_SHORT).show();
            }





        }
    }

    public class LoginTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.UserLogin(etEmail.getText().toString(),etPassword.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                if(s.contains("forbidden"))
                {
                    JSONObject jsonObj = new JSONObject(s);
                    String msg = jsonObj.getString("msg").toString();
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");
                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
                    for (int i = 0; i < getData.length(); i++) {
                        JSONObject c = getData.getJSONObject(i);
                        String user_id = c.getString("user_id");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user_id",user_id);
                        String user_ref_id = c.getString("user_ref_id");
                        editor.putString("user_ref_id",user_ref_id);
                        String user_first_name = c.getString("user_first_name");
                        editor.putString("user_first_name",user_first_name);
                        String user_last_name = c.getString("user_last_name");
                        editor.putString("user_last_name",user_last_name);
                        String user_mobile = c.getString("user_mobile");
                        String user_email = c.getString("user_email");
                        editor.putString("user_email",user_email);
                        editor.putString("user_mobile",user_mobile);
                        String country = c.getString("user_add_cont");
                        editor.putString("user_add_cont",country);
                        String state = c.getString("user_add_state");
                        editor.putString("user_add_state",state);
                        String city = c.getString("user_add_city");
                        editor.putString("user_add_city",city);
                        String street = c.getString("user_add_st");
                        editor.putString("user_add_st",street);
                        String ou_id = c.getString("ou_id");
                        editor.putString("ou_id",ou_id);
                        String user_role = c.getString("role_name");
                        editor.putString("role_name",user_role);
                        editor.putBoolean("isUserLoggedIn",true);
                        editor.commit();

                        Log.d("userdetais  ", user_id + "-------" + user_first_name + "-----" + ou_id);


                        // AppConsts.ShowToast(getApplicationContext() , user_id + "-------" + ou_id + "-------" + user_first_name + "-------" + user_last_name + "-------" + user_mobile+ "-------" + country + "-------" + state + "-------" + city + "-------" + street+ "-------" + usernameInput+ "-------" + user_role );
                    }
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });

                }

            }catch (Exception e)
            {
        Log.e("Exception Here",e.toString());
            }





        }
    }

    public class SilentLoginTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.DoSilentLogin(android_id);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                if(s.contains("forbidden"))
                {

                }
                else {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");
                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
                    for (int i = 0; i < getData.length(); i++) {
                        JSONObject c = getData.getJSONObject(i);
                        String user_id = c.getString("user_id");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user_id",user_id);
                        String user_ref_id = c.getString("user_ref_id");
                        editor.putString("user_ref_id",user_ref_id);
                        String user_first_name = c.getString("user_first_name");
                        editor.putString("user_first_name",user_first_name);
                        String user_last_name = c.getString("user_last_name");
                        editor.putString("user_last_name",user_last_name);
                        String user_mobile = c.getString("user_mobile");
                        String user_email = c.getString("user_email");
                        editor.putString("user_email",user_email);
                        editor.putString("user_mobile",user_mobile);
                        String country = c.getString("user_add_cont");
                        editor.putString("user_add_cont",country);
                        String state = c.getString("user_add_state");
                        editor.putString("user_add_state",state);
                        String city = c.getString("user_add_city");
                        editor.putString("user_add_city",city);
                        String street = c.getString("user_add_st");
                        editor.putString("user_add_st",street);
                        String ou_id = c.getString("ou_id");
                        editor.putString("ou_id",ou_id);
                        String user_role = c.getString("role_name");
                        editor.putString("role_name",user_role);
                        editor.putBoolean("isUserLoggedIn",true);
                        editor.commit();

                        Log.d("userdetais  ", user_id + "-------" + user_first_name + "-----" + ou_id);
                        // AppConsts.ShowToast(getApplicationContext() , user_id + "-------" + ou_id + "-------" + user_first_name + "-------" + user_last_name + "-------" + user_mobile+ "-------" + country + "-------" + state + "-------" + city + "-------" + street+ "-------" + usernameInput+ "-------" + user_role );
                    }

                }

            }catch (Exception e)
            {

            }





        }
    }
    public void showForgotPassword()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_forgot_password_popup, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);


        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);

        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();
        AlertDialog currentDialog =dialog;
        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new ForgotPasswordTask().execute();
                }
//                else
//                {
//                    new UpdateInfoTask().execute();
//
//                }

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    public class ForgotPasswordTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.ForgotPassword(etEmail.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                JSONObject jsonObj = new JSONObject(s);
                String msg = jsonObj.getString("msg").toString();
                if(s.contains("forbidden"))
                {

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {
                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                }

            }catch (Exception e)
            {
                Log.e("Exception Here",e.toString());
            }





        }
    }

}
