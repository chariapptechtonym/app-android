package org.codearray.crecentrelife.MainHome.MyNetworks;

import java.util.ArrayList;

/**
 * Created by Haseeb on 4/14/18.
 */

public class ContactsList {

    public ArrayList<Contacts> contactArrayList;

    ContactsList(){

        contactArrayList = new ArrayList<Contacts>();
    }

    public int getCount(){

        return contactArrayList.size();
    }

    public void addContact(Contacts contact){
        contactArrayList.add(contact);
    }

    public  void removeContact(Contacts contact){
        contactArrayList.remove(contact);
    }

    public Contacts getContact(int id){

        for(int i=0;i<this.getCount();i++){
            if(Integer.parseInt(contactArrayList.get(i).id)==id)
                return contactArrayList.get(i);
        }

        return null;
    }
}
