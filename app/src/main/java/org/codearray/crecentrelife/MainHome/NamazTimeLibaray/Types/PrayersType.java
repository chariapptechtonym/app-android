package org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types;

/**
 * Created by Haseeb on 4/4/18.
 */

public enum PrayersType {

    FAJR(0),
    SUNRISE(1),
    ZUHR(2),
    ASR(3),
    MAGHRIB(4),
    ISHA(5);
    private int index;

    public int getIndex() {
        return index;
    }

    PrayersType(int index) {
        this.index = index;
    }
}
