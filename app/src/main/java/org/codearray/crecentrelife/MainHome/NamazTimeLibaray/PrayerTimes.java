package org.codearray.crecentrelife.MainHome.NamazTimeLibaray;

/**
 * Created by Haseeb on 4/4/18.
 */


import org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType;

import java.util.Date;

import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.MILLIS_IN_SECOND;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.MINUTE_IN_HOUR;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.SECOND_IN_MINUTE;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType.ASR;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType.MAGHRIB;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType.SUNRISE;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType.ZUHR;
import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static java.lang.Math.ceil;

public class PrayerTimes {

    private boolean useSecond = false;
    private double[] prayerTimes;
    private long millis;

    PrayerTimes(long millis, double... prayerTimes) {
        this.prayerTimes = prayerTimes;
        this.millis = millis;

        if (prayerTimes[SUNRISE.getIndex()] == NEGATIVE_INFINITY || prayerTimes[MAGHRIB.getIndex()] == POSITIVE_INFINITY) {
            prayerTimes[ZUHR.getIndex()] = POSITIVE_INFINITY;
            prayerTimes[ASR.getIndex()] = POSITIVE_INFINITY;
        }
    }

    public Date getPrayTime(PrayersType payersType) {
        double time = prayerTimes[payersType.getIndex()];
        if (time == POSITIVE_INFINITY || time == NEGATIVE_INFINITY) {
            return null;
        }
        // negative hours will raise exception
        if (isUseSecond())
            return new Date(this.millis + (long) ceil(time * MINUTE_IN_HOUR * SECOND_IN_MINUTE) * MILLIS_IN_SECOND);
        else
            return new Date(this.millis + (long) ceil(time * MINUTE_IN_HOUR) * SECOND_IN_MINUTE * MILLIS_IN_SECOND);
    }

    public Time getTimeinHoursAndMinutesAndSecounds(int timeName) {
        double time = prayerTimes[timeName];
        if (time == POSITIVE_INFINITY || time == NEGATIVE_INFINITY)
            return null;
        if (isUseSecond())
            return new Time(time);
        else
            return new Time(ceil(time * MINUTE_IN_HOUR) / SECOND_IN_MINUTE);
    }



    public boolean isUseSecond() {
        return useSecond;
    }

    public void setUseSecond(boolean useSecond) {
        this.useSecond = useSecond;
    }



}
