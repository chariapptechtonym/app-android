package org.codearray.crecentrelife.MainHome.UserHome.Family;

import android.app.Activity;
import android.content.Intent;
import android.content.ReceiverCallNotAllowedException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.BenifitsAdapter;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

/**
 * Created by Haseeb on 4/13/18.
 */

public class FamilyAdpater extends ArrayAdapter<String>  {
    String[] recp_id;
    String[] recp_firstName;
    String[] recp_lastName;
    String[] gender;
    String[] recp_class;
    String[] recp_dob;
    String[] recp_city;
    String[] recp_category;




    private final Activity context;

    public FamilyAdpater(Activity context, String[] recp_id, String[] recp_firstName, String[] recp_lastName, String[] recp_dob, String[] gender, String[] recp_class, String[] recp_category, String[] recp_city) {
        super(context, R.layout.family_view_list, recp_lastName);
        this.context = context;
        this.recp_id = recp_id;
        this.recp_firstName = recp_firstName;
        this.recp_lastName = recp_lastName;
        this.recp_dob = recp_dob;
        this.gender = gender;
        this.recp_class = recp_class;
        this.recp_category = recp_category;
        this.recp_city = recp_city;
    }

    public class LoadViews
    {
        TextView username;
        TextView gender;
        TextView age;
        TextView recpclass;
        TextView city;
        TextView status;
        ImageView userPhoto;
//        TextView username;
//        TextView contact;
//        TextView dob;
//

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FamilyAdpater.LoadViews viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.family_view_list, null);
            viewHolder = new FamilyAdpater.LoadViews();
            viewHolder.username = (TextView) convertView.findViewById(R.id.recpname);
            viewHolder.userPhoto = (ImageView) convertView.findViewById(R.id.recp_profile_pic);
            viewHolder.gender = (TextView) convertView.findViewById(R.id.gender_type_recp);
            viewHolder.age = (TextView) convertView.findViewById(R.id.age_recp);
            viewHolder.recpclass = (TextView) convertView.findViewById(R.id.recp_class);
            viewHolder.city = (TextView) convertView.findViewById(R.id.city_recp);
            viewHolder.status = (TextView) convertView.findViewById(R.id.recp_status);
            convertView.setTag(viewHolder);
        }else
        {
            viewHolder = (FamilyAdpater.LoadViews) convertView.getTag();
        }

        viewHolder.username.setText(recp_firstName[position]+" "+recp_lastName[position]);
        viewHolder.gender.setText(gender[position]);
        viewHolder.age.setText("10 Years");
        viewHolder.recpclass.setText(recp_class[position]);
        viewHolder.city.setText(recp_city[position]);
        viewHolder.status.setText(recp_category[position]);

        viewHolder.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //AppConsts.ShowToast(context , recp_id[position]);
                Intent a = new Intent(context , RecipientUpdates.class);
                a.putExtra("name", recp_firstName[position]+" "+recp_lastName[position] );
                a.putExtra("gender" ,gender[position] );
                a.putExtra("age" , "10 years");
                a.putExtra("city" , recp_city[position]);
                a.putExtra("status" , recp_category[position]);
                a.putExtra("id" , recp_id[position]);
                context.startActivity(a);
                //context.finish();
            }
        });


        return  convertView;
    }



}
