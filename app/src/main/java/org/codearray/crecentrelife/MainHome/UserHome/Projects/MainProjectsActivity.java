package org.codearray.crecentrelife.MainHome.UserHome.Projects;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.interfaces.HttpResponseCallback;
import com.braintreepayments.api.internal.HttpClient;
import com.braintreepayments.api.models.PaymentMethodNonce;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.Family.FamilyActivity;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainProjectsActivity extends AppCompatActivity {

    String stringNonce;
    Spinner spinner_2;
    private ArrayAdapter<String> aspnProjects;
    private List<String> allprojects;
   // Button donate_btn;
   String projectId;
    TextView goBackHomePro;


    ArrayList<String> projectList;
    ArrayList<ProjectDetail> project;

    String programName , programid;


    ConstraintLayout orginalll;       // the project breif one


    EditText ocpPrgram , othersprogram;
    RadioButton btn1, btn2;
    private RadioGroup radioSexGroup;
    ImageView donateBtn;

    TextView projectBreif;

    String amountDonated;
    int testValue;


    //payment settings
    final int REQUEST_CODE = 1;
    final String get_token = "https://payments.chariapp.com/payments/main.php";
    final String send_payment_details = "http://payments.chariapp.com/payments/mycheckout.php";
    String token;
    HashMap<String, String> paramHash;
    SharedPreferences prefs;
    String intentResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_projects);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        orginalll = (ConstraintLayout) findViewById(R.id.view_lid);
        orginalll.setVisibility(View.INVISIBLE);



        projectBreif = (TextView) findViewById(R.id.project_brief);
        goBackHomePro = (TextView) findViewById(R.id.goBackPro);

        //ocpPrgram = (EditText) findViewById(R.id.project_amount_paid);
        othersprogram = (EditText) findViewById(R.id.donation_ammount_other_pro);

        othersprogram.setEnabled(false);

        donateBtn = (ImageView) findViewById(R.id.donate_btn_projects);

        //radio groups
     //   btn1 = (RadioButton) findViewById(R.id.project_monthly);
      //  btn2 = (RadioButton) findViewById(R.id.project_yearly);
     //   radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);



        Intent getany = getIntent();
        intentResult = getany.getStringExtra("family");



        project = new ArrayList<ProjectDetail>();
        projectList = new ArrayList<String>();


//        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                switch (i)
//                {
//                    case R.id.project_monthly:
//                        ocpPrgram.setText("20");
//                        break;
//
//                    case R.id.project_yearly:
//                        ocpPrgram.setText("100");
//                }
//            }
//        });


        new ProjectTask().execute();

        spinner_2 = (Spinner) findViewById(R.id.project_spin);

        if(intentResult != null && !intentResult.isEmpty())
        {
            spinner_2.setEnabled(false);
        }
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
//                this, R.array.projects, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner_2.setAdapter(adapter);
//
//        spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if(i == 1){
//                    AppConsts.ShowToast(MainProjectsActivity.this, "Okay Fine");
//                    ll.setVisibility(View.VISIBLE);
//                }
//                else{
//                    ll.setVisibility(View.INVISIBLE);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        donateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(testValue == 0){
//                    amountDonated = ocpPrgram.getText().toString();
//
//                }else{
//                    amountDonated = othersprogram.getText().toString();
//                }

                if(!_validate()){
                    return;
                }else {
                    onBraintreeSubmit();
                }



              //  AppConsts.ShowToast(getApplicationContext() , amountDonated);

            }
        });

        goBackHomePro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent closeNet = new Intent(MainProjectsActivity.this, MainActivity.class);
                startActivity(closeNet);
                finish();
            }
        });


    }

    boolean _validate()
    {
        boolean valid = true;
        // _name = name.getText().toString();
        amountDonated = othersprogram.getText().toString();

        if (amountDonated.isEmpty()) {
            othersprogram.setError("please enter the donation amount");
            valid = false;
        } else {
            othersprogram.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent closeNet = new Intent(MainProjectsActivity.this, MainActivity.class);
//        startActivity(closeNet);
        finish();
    }


    //get project task
    //login task goes here.
    public class ProjectTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog = new MaterialDialog.Builder(MainProjectsActivity.this)

                    .content("Please wait...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.GetActiveProjects("1");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (nDialog.isShowing())
                nDialog.dismiss();

            try {
                JSONObject jsonObj = new JSONObject(s);
                JSONArray getData = jsonObj.getJSONArray("result");

              //  AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));

                if (getData.length() > 0) {
                    for (int i = 0; i < getData.length(); i++) {
                        JSONObject c = getData.getJSONObject(i);

                        ProjectDetail projectloop = new ProjectDetail();
                        projectloop.setProject_id(c.getString("project_id"));
                        projectloop.setProject_name(c.getString("project_name"));
                        projectloop.setProject_brief(c.getString("project_breif"));


                        project.add(projectloop);
                        projectList.add(c.getString("project_name"));

                    }

                    spinner_2
                            .setAdapter(new ArrayAdapter<String>(MainProjectsActivity.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    projectList));

                    spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            projectBreif.setText(project.get(i).getProject_brief());
                            programName = project.get(i).getProject_name();
                            programid = project.get(i).getProject_id();
                            //txtrank.setText("Rank : "
                                   // + world.get(position).getRank());
                            //AppConsts.ShowToast(getApplicationContext() , project.get(i).getProject_id());
                            //AppConsts.ShowToast(getApplicationContext() , project.get(i).getProject_brief());

                            if(i == 0){
                                testValue = 1;
                               // subll.setVisibility(View.VISIBLE);
                                orginalll.setVisibility(View.VISIBLE);

                                //amountDonated = ocpPrgram.getText().toString();
                            }else{
                                testValue = 1;
                                //subll.setVisibility(View.INVISIBLE);
                                orginalll.setVisibility(View.VISIBLE);
                                //amountDonated = othersprogram.getText().toString();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }

            } catch (Exception e) {

            }

            new TokenRequest().execute();

        }
    }


    //get payment token....!!
    private class TokenRequest extends AsyncTask {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(MainProjectsActivity.this, android.R.style.Theme_DeviceDefault_Dialog);
            progress.setCancelable(false);
            progress.setMessage("We are contacting our servers for token, Please wait");
            progress.setTitle("Getting token");
            progress.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpClient client = new HttpClient();
            client.get(get_token, new HttpResponseCallback() {
                @Override
                public void success(String responseBody) {
                    Log.d("mylog", responseBody);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          //  Toast.makeText(MainProjectsActivity.this, "Successfully got token", Toast.LENGTH_SHORT).show();
                            othersprogram.setEnabled(true);
                            //llHolder.setVisibility(View.VISIBLE);
                            //etAmount.setVisibility(View.VISIBLE);
                        }
                    });
                    token = responseBody;
                   // Toast.makeText(MainProjectsActivity.this, token, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void failure(Exception exception) {
                    final Exception ex = exception;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          //  Toast.makeText(MainProjectsActivity.this, "Failed to get token: " + ex.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progress.dismiss();




        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                PaymentMethodNonce nonce = result.getPaymentMethodNonce();
                stringNonce = nonce.getNonce();
                Log.d("mylog", "Result: " + stringNonce);
                    projectId = project.get(spinner_2.getSelectedItemPosition()).getProject_id();
                    paramHash = new HashMap<>();
                    paramHash.put("amount", amountDonated);
                    paramHash.put("nonce", stringNonce);
                    paramHash.put("ou_id",prefs.getString("ou_id",""));
                    paramHash.put("project_id",projectId);
                    new SendDataToServerTask().execute();


            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
                Log.d("mylog", "user canceled");
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Log.d("mylog", "Error : " + error.toString());
            }
        }
    }

    //payment complete processing....!!
    public void onBraintreeSubmit() {
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(token);

        dropInRequest.collectDeviceData(true);
        startActivityForResult(dropInRequest.getIntent(this), REQUEST_CODE);
    }

    private void sendPaymentDetail() {
        RequestQueue queue = Volley.newRequestQueue(MainProjectsActivity.this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, send_payment_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("--------------------------------");
                        System.out.println(response);

//                        String[] AddressParts = response.split("#");
//                        String transid = AddressParts[0];
//                        System.out.println(transid);

                        final Matcher matcher = Pattern.compile("#").matcher(response);
                        if(matcher.find()){
                            System.out.println(response.substring(matcher.end()).trim());
                        }


                        System.out.println("--------------------------------");
                        if(response.contains("success"))
                        {

                            if(intentResult != null && !intentResult.isEmpty()) {
                                Toast.makeText(MainProjectsActivity.this, "Transaction successful Family", Toast.LENGTH_LONG).show();
                                Intent showRecpt = new Intent(MainProjectsActivity.this, ProjectReciptActivity.class);
                                showRecpt.putExtra("projectname", programName);
                                showRecpt.putExtra("amount", amountDonated);
                                showRecpt.putExtra("isfamily", "yes");
                                showRecpt.putExtra("programid", programid);
                                startActivity(showRecpt);
                                finish();
                            }
                            else {
                                Toast.makeText(MainProjectsActivity.this, "Transaction successful", Toast.LENGTH_LONG).show();
                                Intent showRecpt = new Intent(MainProjectsActivity.this, ProjectReciptActivity.class);
                                showRecpt.putExtra("projectname", programName);
                                showRecpt.putExtra("amount", amountDonated);
                                showRecpt.putExtra("isfamily", "no");
                                showRecpt.putExtra("programid",  programid);
                                startActivity(showRecpt);
                                finish();
                            }
                        }
                        else Toast.makeText(MainProjectsActivity.this, "Transaction failed", Toast.LENGTH_LONG).show();
                        Log.d("mylog", "Final Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("mylog", "Volley error : " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                if (paramHash == null)
                    return null;
                Map<String, String> params = new HashMap<>();
                for (String key : paramHash.keySet()) {
                    params.put(key, paramHash.get(key));
                    Log.d("mylog", "Key : " + key + " Value : " + paramHash.get(key));
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }



    public class SendDataToServerTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(MainProjectsActivity.this)
                    .title("Please Wait")
                    .content("Processing your request")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.sendDataToServer(amountDonated,stringNonce,prefs.getString("ou_id",""),projectId);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();

            try
            {
                if(s.contains("success"))
                {
                    if(intentResult != null && !intentResult.isEmpty()) {
                        Toast.makeText(MainProjectsActivity.this, "Transaction successful Family", Toast.LENGTH_LONG).show();
                        Intent showRecpt = new Intent(MainProjectsActivity.this, ProjectReciptActivity.class);
                        showRecpt.putExtra("projectname", programName);
                        showRecpt.putExtra("amount", amountDonated);
                        showRecpt.putExtra("isfamily", "yes");
                        showRecpt.putExtra("programid", programid);
                        startActivity(showRecpt);
//                        finish();
                    }
                    else {
                        Toast.makeText(MainProjectsActivity.this, "Transaction successful", Toast.LENGTH_LONG).show();
                        Intent showRecpt = new Intent(MainProjectsActivity.this, ProjectReciptActivity.class);
                        showRecpt.putExtra("projectname", programName);
                        showRecpt.putExtra("amount", amountDonated);
                        showRecpt.putExtra("isfamily", "no");
                        showRecpt.putExtra("programid",  programid);
                        startActivity(showRecpt);
//                        finish();
                    }
                }
                else {
                    MaterialDialog.Builder dialog = new MaterialDialog.Builder(MainProjectsActivity.this)
                            .title("Error..!!")
                            .content("Transaction Failed")
                            .positiveText("Ok");
                    dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            //// TODO
                        }
                    });
                    dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }

            }catch (Exception e)
            {

            }





        }
    }
}
