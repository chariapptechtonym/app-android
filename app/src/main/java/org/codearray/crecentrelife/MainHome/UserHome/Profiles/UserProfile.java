package org.codearray.crecentrelife.MainHome.UserHome.Profiles;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.SettingsFrag;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

import java.nio.file.attribute.UserPrincipal;
import java.util.HashMap;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class UserProfile extends AppCompatActivity implements MaterialTabListener {



    MaterialTabHost tabHost;
    ViewPager pager;
    ViewPagerAdapter adapter;

    String userrole;
    LoginSession loginSession;

    String items[] = {"Profile", "Settings"};
   // String items_o[]  ={"Profile" , "Scan Coupons" , "Settings"};

    ImageView logbtn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        loginSession = new LoginSession(this);


       // logbtn = (ImageView) findViewById(R.id.lg_btn);

        HashMap<String ,String> detail = loginSession.getUserDetails();
        userrole =detail.get(LoginSession.user_role);

        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager );




        // init view pager
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);

            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent startprofile = new Intent(UserProfile.this, MainActivity.class);
//        startActivity(startprofile);
        finish();
    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View layoutView = inflater.inflate(R.layout.activity_user_profile,
//                container, false);
//
//
//        tabHost = (MaterialTabHost) layoutView.findViewById(R.id.tabHost);
//        pager = (ViewPager) layoutView.findViewById(R.id.pager);
//
//        // init view pager
//        adapter = new ViewPagerAdapter(getFragmentManager());
//        pager.setAdapter(adapter);
//        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
//            @Override
//            public void onPageSelected(int position) {
//                // when user do a swipe the selected tab change
//                tabHost.setSelectedNavigationItem(position);
//
//            }
//        });
//
//        // insert all tabs from pagerAdapter data
//        for (int i = 0; i < adapter.getCount(); i++) {
//            tabHost.addTab(
//                    tabHost.newTab()
//                            .setText(adapter.getPageTitle(i))
//                            .setTabListener(this)
//            );
//
//        }
//
//
//        return layoutView;
//    }



    @Override
    public void onTabSelected(MaterialTab tab) {

    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {


            switch (num) {
                case 0: // Fragment # 0 - This will show image
                   return ProfileFrag.init(num);
                    //return null;
                case 1: // Fragment # 1 - This will show image
                    return SettingsFrag.init(num);
                default:// Fragment # 2-9 - Will show list
                    //return AllCollections.init(num);
                    return null;
            }
        }

        @Override
        public int getCount() {

            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return items[position];
        }

    }
}
