package org.codearray.crecentrelife.MainHome.UserRegistration;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.MyNetworks.AddMember;
import org.codearray.crecentrelife.MainHome.MyNetworks.NetworkMainActivity;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.Utils.ConnectivityReceiver;
import org.codearray.crecentrelife.MainHome.Utils.PermissionUtils;
import org.codearray.crecentrelife.R;

import java.io.IOException;
import java.util.Random;

public class UserRegActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView _regBtn;
    String _firstname, _lastname, _email, _password, _phone;
    EditText firstname, lastname, email, password, phone;

    private boolean mPermissionDenied = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    public static final String upper = "AA";
    public static final String digits = "0123456789";
    String refId;


    private  void initViews()
    {
      firstname = (EditText) findViewById(R.id.first_name);
      lastname = (EditText) findViewById(R.id.last_name);
      email = (EditText) findViewById(R.id.email_user);
      password = (EditText) findViewById(R.id.password_user);
      phone = (EditText) findViewById(R.id.phone_user);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reg);




        initViews();

        _regBtn = (ImageView) findViewById(R.id.reg_button_id);
        _regBtn.setOnClickListener(this);

        String locale = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry();
       // AppConsts.ShowToast(getApplicationContext() , locale);

        Random rand = new Random();
        int  n = rand.nextInt(9999) + 1000;
        refId = "AA"+String.valueOf(n);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent goHome = new Intent(UserRegActivity.this, UserLoginActivity.class);
        startActivity(goHome);
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.

        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }



    boolean _validate()
    {
        boolean valid = true;
        // _name = name.getText().toString();
        _firstname = firstname.getText().toString();
        _lastname = lastname.getText().toString();
        _email = email.getText().toString();
        _password = password.getText().toString();
        _phone = phone.getText().toString();

        if (_firstname.isEmpty()) {
            firstname.setError("please provide your First Name");
            valid = false;
        } else {
            firstname.setError(null);
        }

        if (_lastname.isEmpty()) {
            lastname.setError("please provide your Last Name");
            valid = false;
        } else {
            lastname.setError(null);
        }


        if (_email.isEmpty()) {
            email.setError("please provide your Email Address");
            valid = false;
        } else {
            email.setError(null);
        }


        if (_password.isEmpty()) {
            password.setError("please provide your Password");
            valid = false;
        } else {
            password.setError(null);
        }


        return valid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.reg_button_id:
               // boolean isConnected = ConnectivityReceiver.isConnected();
                //if(isConnected){
                    if(!_validate()){
                        return;
                    }else{
                        new RegistrationTask().execute();
                    }
               // }else{
               //     AppConsts.ShowToast(getApplicationContext() , "Please check your Internet Connection");
               // }

                break;
        }
    }


    public class RegistrationTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(UserRegActivity.this)
                    .title("Please Wait")
                    .content("Registering your Account")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.UserRegistration(_firstname, _lastname, _email, _password, _phone, refId);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();



            if(s.contains("Failed")){
                MaterialDialog.Builder dialog = new MaterialDialog.Builder(UserRegActivity.this)
                        .title("Account Registeration Failed")
                        .content("The email is already in use. Please try with an other email address or Login to your account")
                        .positiveText("Login")
                        .negativeText("Try Another Email");
                dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //// TODO
                        AppConsts.ShowToast(getApplicationContext() , "Done");
                        Intent closeNet = new Intent(UserRegActivity.this, UserLoginActivity.class);
                        startActivity(closeNet);
                        finish();
                    }
                });
                dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
            else{
                MaterialDialog.Builder dialog = new MaterialDialog.Builder(UserRegActivity.this)
                        .title("Account Registered")
                        .content("Your account has been registered Successfully. Please check your email to verify your account and Login")
                        .positiveText("OK");
                dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //// TODO
                        AppConsts.ShowToast(getApplicationContext() , "Done");
                        Intent closeNet = new Intent(UserRegActivity.this, UserLoginActivity.class);
                        startActivity(closeNet);
                        finish();
                    }
                });
                dialog.show();
            }






        }
    }
}
