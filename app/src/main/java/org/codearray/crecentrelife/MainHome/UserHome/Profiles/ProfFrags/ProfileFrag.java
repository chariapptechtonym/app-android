package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileUtils.CityList;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileUtils.CountryDetails;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileUtils.RegionDetails;
import org.codearray.crecentrelife.MainHome.UserHome.Projects.ProjectDetail;
import org.codearray.crecentrelife.MainHome.UserHome.Projects.ProjectReciptActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfileFrag extends Fragment {


    LoginSession loginSession;
    String name, phone , email, count, city , state, street , userid , _ouid , _user_ref_id , userole, lastname;
    TextView _name , _phone , _email , _count, _city , _state , _street , _userid , _address;

    ImageView submitFeedbackbtm;
    ImageView submitIC;

    EditText textFeed;

    String feedbackText;
    SharedPreferences prefs;
    AlertDialog dialogBuilder;

    ImageView lgBtn;

    int fragNum;
    public static ProfileFrag init(int val) {
        ProfileFrag truitonList = new ProfileFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        fragNum = getArguments() != null ? getArguments().getInt("val") : 1;
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_profile,
                container, false);

        _userid  = (TextView) layoutView.findViewById(R.id.refer_id);
        _address = (TextView) layoutView.findViewById(R.id.prof_add);
        _name = (TextView) layoutView.findViewById(R.id.prof_username);
        _phone = (TextView) layoutView.findViewById(R.id.prof_userphone);
        _email = (TextView) layoutView.findViewById(R.id.prof_useremail);
        _count = (TextView) layoutView.findViewById(R.id.prof_count);
        _city = (TextView) layoutView.findViewById(R.id.prof_city);
        _state = (TextView) layoutView.findViewById(R.id.prof_state);
        _street = (TextView) layoutView.findViewById(R.id.prof_street);
        textFeed = (EditText) layoutView.findViewById(R.id.feedbac_text);
        submitFeedbackbtm = (ImageView) layoutView.findViewById(R.id.sbt_btn);
        submitIC = (ImageView) layoutView.findViewById(R.id.edit_btn_prof);

        lgBtn = (ImageView) layoutView.findViewById(R.id.logout_btn_new);

        loginSession = new LoginSession(getActivity());

        HashMap<String ,String> detail = loginSession.getUserDetails();
        name = prefs.getString("user_first_name","");//detail.get(LoginSession.firstname);
        lastname = prefs.getString("user_last_name","");//detail.get(LoginSession.lastname);
        email =prefs.getString("user_email","");//detail.get(LoginSession.email);
        count =prefs.getString("user_add_cont","");//detail.get(LoginSession.country);
        city =prefs.getString("user_add_city","");//detail.get(LoginSession.city);
        state =prefs.getString("user_add_state","");//detail.get(LoginSession.state);
        street =prefs.getString("user_add_st","");//detail.get(LoginSession.street);
        phone =prefs.getString("user_mobile","");//detail.get(LoginSession.phonenum);
        userid =prefs.getString("user_id","");//detail.get(LoginSession.userid);
        _ouid =prefs.getString("ou_id","");//detail.get(LoginSession.ouid);
        _user_ref_id =prefs.getString("user_ref_id","");//detail.get(LoginSession.ref_id);
        userole = prefs.getString("role_name","");//detail.get(LoginSession.user_role);

        if(lastname.length() != 0)
        {
            _name.setText(name+" "+lastname);
        }
        else {
            _name.setText(name);
        }
        _phone.setText(phone);
        _email.setText(email);
        _count.setText(count);

        _city.setText(city);
        _state.setText(state);
        _street.setText(street);
        _userid.setText("Customer ID\n"+_user_ref_id);

        String s = city+", "+count;
        _address.setText(s);

        submitFeedbackbtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackText = textFeed.getText().toString();
                if(!_validate()){
                    return;
                }else{
                    new RequestMemberTask().execute();
                }
            }
        });


        lgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginSession.deleteCode();
//                Intent logout = new Intent(getActivity() , UserLoginActivity.class);
//                startActivity(logout);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString("user_ref_id","");
                edit.commit();
                getActivity().finish();
            }
        });

        submitIC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calibrateDialog();
            }
        });

//        lgBtn.setVisibility(View.GONE);
        return layoutView;
    }

    String selectedCount;
    String selectCountName;
    String selectedRegion;
    String selectedCites;
    String StreetAdd = "";
    private void calibrateDialog(){
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View calibrateView = layoutInflater.inflate(R.layout.profile_edit_dialog, null);


        ImageView btnClose = (ImageView) calibrateView.findViewById(R.id.close);
        final Spinner countries = (Spinner) calibrateView.findViewById(R.id.edit_country);
        final Spinner states = (Spinner) calibrateView.findViewById(R.id.edit_state);
        final Spinner citesdrop = (Spinner) calibrateView.findViewById(R.id.edit_city);
        final EditText newStreetAdd = (EditText) calibrateView.findViewById(R.id.edit_street_address);

        dialogBuilder = new AlertDialog.Builder(getActivity())
                .setView(calibrateView)
                .setTitle("Profile Updates")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        dialogBuilder.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = ((AlertDialog) dialogBuilder).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        loginSession.deleteCode();
                        StreetAdd = newStreetAdd.getText().toString();

                        loginSession.createLoginSession(userid , _ouid , name , lastname , phone , selectCountName , selectedCites , selectedRegion , StreetAdd , email, userole ,_user_ref_id);
                        _count.setText(selectCountName);
                        _state.setText(selectedRegion);
                        _city.setText(selectedCites);
                        _street.setText(StreetAdd);

                        //Dismiss once everything is OK.
                        dialogBuilder.dismiss();
                    }
                });
            }
        });

//        dialogBuilder = new PopUpDialog.Builder(getActivity()).create();
//        //dialogBuilder.setTitle("How to calibrate");
//        dialogBuilder.setIcon(R.mipmap.ic_launcher);
//        dialogBuilder.setView(calibrateView);
//        dialogBuilder.setButton("Save" , null);




//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //dialogBuilder.dismiss();
//            }
//        });

        //get countries
        final ArrayList<String> countryList  = new ArrayList<String>();         //this show data on spinner
        final ArrayList<CountryDetails> country = new ArrayList<CountryDetails>(); //this is the selection

        //get regions
        final ArrayList<String> regionList  = new ArrayList<String>();
        final ArrayList<RegionDetails> region = new ArrayList<RegionDetails>();

        //get regions
        final ArrayList<String> cityList  = new ArrayList<String>();
        final ArrayList<CityList> city = new ArrayList<CityList>();



        dialogBuilder.show();

        class GetCity extends AsyncTask<Void, Void, String> {
            private MaterialDialog nDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                nDialog =  new MaterialDialog.Builder(getActivity())
                        .content("Please wait...")
                        .progress(true, 0)
                        .backgroundColor(Color.WHITE)
                        .contentColor(Color.BLACK)
                        .titleColor(Color.BLACK)
                        .dividerColor(Color.BLACK)
                        .widgetColor(Color.parseColor("#55B0CF"))
                        .show();
            }
            @Override
            protected String doInBackground(Void... voids) {
                String responseString = null;
                try {
                    responseString = HttpRequests.GetCities(selectedCount , selectedRegion);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return responseString;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                nDialog.dismiss();
                try {

                    if(s.isEmpty()){

                    }else {
                        JSONArray json = new JSONArray(s);
                        //AppConsts.ShowToast(getActivity(), String.valueOf(json.length()));
                        // Getting Countries
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject c = json.getJSONObject(i);

                            CityList projectloop = new CityList();
                            projectloop.setCity(c.getString("city"));

                            // projectloop.setRegionName(c.getString("region"));
                            //projectloop.setRegionCountCode(c.getString("country"));

                            city.add(projectloop);                                   //this is the selection
                            cityList.add(c.getString("city"));       // this show the data
                        }
                        citesdrop.setAdapter(new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                cityList));
                        citesdrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                               // AppConsts.ShowToast(getActivity(), city.get(i).getCity());
                                selectedCites = city.get(i).getCity();
                                //new GetCountries().execute();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }


        class GetRegion extends AsyncTask<Void, Void, String> {
            private MaterialDialog nDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                nDialog =  new MaterialDialog.Builder(getActivity())
                        .content("Please wait...")
                        .progress(true, 0)
                        .backgroundColor(Color.WHITE)
                        .contentColor(Color.BLACK)
                        .titleColor(Color.BLACK)
                        .dividerColor(Color.BLACK)
                        .widgetColor(Color.parseColor("#55B0CF"))
                        .show();
            }
            @Override
            protected String doInBackground(Void... voids) {
                String responseString = null;
                try {
                    responseString = HttpRequests.GetRegions(selectedCount);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return responseString;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                nDialog.dismiss();
                try {
                    JSONArray json = new JSONArray(s);
                    //AppConsts.ShowToast(getActivity(), String.valueOf(json.length()));
                    // Getting Countries
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject c = json.getJSONObject(i);

                        RegionDetails projectloop = new RegionDetails();
                        projectloop.setRegionName(c.getString("region"));
                        projectloop.setRegionCountCode(c.getString("country"));

                        region.add(projectloop);                                   //this is the selection
                        regionList.add(c.getString("region"));       // this show the data
                    }
                    states.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            regionList));
                    states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                           // AppConsts.ShowToast(getActivity(), region.get(i).getRegionName());
                            selectedRegion = region.get(i).getRegionName();
                            if(city.size()>0){
                                city.clear();
                                cityList.clear();
                            }
                            new GetCity().execute();
                            //new GetCountries().execute();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }


        class GetCountries extends AsyncTask<Void, Void, String> {
            private MaterialDialog nDialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                nDialog =  new MaterialDialog.Builder(getActivity())
                        .content("Please wait...")
                        .progress(true, 0)
                        .backgroundColor(Color.WHITE)
                        .contentColor(Color.BLACK)
                        .titleColor(Color.BLACK)
                        .dividerColor(Color.BLACK)
                        .widgetColor(Color.parseColor("#55B0CF"))
                        .show();
            }
            @Override
            protected String doInBackground(Void... voids) {
                String responseString = null;
                try {
                    responseString = HttpRequests.GetCountriesList();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return responseString;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                nDialog.dismiss();
                try {

                    if(s.isEmpty()){

                    }else {
                        JSONArray json = new JSONArray(s);
                        //AppConsts.ShowToast(getActivity(), String.valueOf(json.length()));
                        // Getting Countries
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject c = json.getJSONObject(i);

                            CountryDetails projectloop = new CountryDetails();
                            projectloop.setCountryName(c.getString("name"));
                            projectloop.setCountryCode(c.getString("code"));

                            country.add(projectloop);                                   //this is the selection
                            countryList.add(c.getString("name"));       // this show the data
                        }
                        countries.setAdapter(new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                countryList));
                        countries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                // AppConsts.ShowToast(getActivity(), country.get(i).getCountryCode());
                                selectedCount = country.get(i).getCountryCode();
                                selectCountName = country.get(i).getCountryName();
                                //new GetCountries().execute();
                                if (region.size() > 0) {
                                    region.clear();
                                    regionList.clear();
                                }

                                new GetRegion().execute();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
            }
        }




        //get listed countries
        new GetCountries().execute();


    }



    boolean _validate()
    {
        boolean valid = true;
        // _name = name.getText().toString();
        feedbackText = textFeed.getText().toString();


        if (feedbackText.isEmpty()) {
            textFeed.setError("please provide your feedback first");
            valid = false;
        } else {
            textFeed.setError(null);
        }

        return valid;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public class RequestMemberTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(getActivity())
                    .title("Please Wait")
                    .content("Submitting your Request")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.SubmitFeedback(_ouid , feedbackText);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();




            MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity())
                    .title("Feedback Sent..!!")
                    .content("We have received your feedback. We always try to make our services more better")
                    .positiveText("OK");
            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog dialog, DialogAction which) {
                    //// TODO

                }
            });
            dialog.show();
            textFeed.setText("");




        }
    }


}
