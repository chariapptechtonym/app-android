package org.codearray.crecentrelife.MainHome.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Haseeb on 4/9/18.
 */

public class LoginSession {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 1;
    private static final String PREF_NAME = "user_login";
    private static final String IS_LOGIN = "IsUserLoggedIn";
    public static final String userid = "userid";
    public static final String ouid = "ouid";
    public static final String ou_id = "ou_id";
    public static final String firstname = "firstname";
    public static final String lastname = "lastname";
    public static final String phonenum = "phonenum";
    public static final String country = "country";
    public static final String city = "city";
    public static final String state = "state";
    public static final String street = "street";
    public static final String email = "email";
    public static final String user_role = "user_role";
    public static final String ref_id = "ref_id";
    //for user auto login feature...

    // Constructor
    public LoginSession(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    //store the data
    public void createLoginSession(String _userid , String _ouid, String _firtname, String _lastname ,
                                   String _phonenum , String _country,
                                   String _city, String _state, String _street, String _email, String _userrole , String _ref_id){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(userid, _userid);
        editor.putString(ouid, _ouid);
        editor.putString(firstname, _firtname);
        editor.putString(lastname, _lastname);
        editor.putString(phonenum, _phonenum);
        editor.putString(country, _country);
        editor.putString(city, _city);
        editor.putString(state, _state);
        editor.putString(street, _street);
        editor.putString(email, _email);
        editor.putString(user_role, _userrole);
        editor.putString(ref_id, _ref_id);
        editor.putString(ou_id,_ouid);
        editor.commit();
    }

    //get the stored data
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(userid, pref.getString(userid, null));
        user.put(ouid, pref.getString(ouid, null));
        user.put(ouid, pref.getString(ou_id, null));
        user.put(firstname, pref.getString(firstname, null));
        user.put(lastname, pref.getString(lastname, null));
        user.put(phonenum, pref.getString(phonenum, null));
        user.put(country, pref.getString(country, null));
        user.put(city, pref.getString(city, null));
        user.put(state, pref.getString(state, null));
        user.put(street, pref.getString(street, null));
        user.put(email, pref.getString(email, null));
        user.put(user_role, pref.getString(user_role, null));
        user.put(ref_id, pref.getString(ref_id, null));
        // return user
        return user;
    }

    //delete the store user info
    public void deleteCode(){
        editor.clear();
        editor.commit();
    }
}
