package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileUtils;

/**
 * Created by Haseeb on 4/19/18.
 */

public class CountryDetails {

    private String countryName;
    private String countryCode;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
