package org.codearray.crecentrelife.MainHome.UserHome.Profiles;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ScannerFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.SettingsFrag;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class SponserProfile extends AppCompatActivity implements MaterialTabListener {


    MaterialTabHost tabHost;
    ViewPager pager;
    ViewPagerAdapter adapter;

    String userrole;
    LoginSession loginSession;

    ImageView logbtn;
    private LocationManager locationManager;
    private String provider;

    //String items[] = {"Profile", "Settings"};
    String items[] = {"Profile", "Scan Coupons", "Settings"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

       // logbtn = (ImageView) findViewById(R.id.lg_btn);

        loginSession = new LoginSession(this);
        HashMap<String, String> detail = loginSession.getUserDetails();
        userrole = detail.get(LoginSession.user_role);



        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager);

        // init view pager
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);

            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (result != null) {
//            if (result.getContents() == null) {
//                // Toast.makeText(getApplicationContext(),    "Cancelled",Toast.LENGTH_LONG).show();
//            } else {
//                //updateText(result.getContents());
//                Toast.makeText(getApplicationContext(), result.getContents(), Toast.LENGTH_LONG).show();
//
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//            // Toast.makeText(getApplicationContext(),    result.getContents(),Toast.LENGTH_LONG).show();
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }


//    private Location getLastKnownLocation() {
//        List<String> providers = locationManager.getProviders(true);
//        Location bestLocation = null;
//
//
//        for (String provider : providers) {
//
//            @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);
//            // Log.d("last known location, provider: %s, location: %s", provider, l);
//
//            if (l == null) {
//                continue;
//            }
//            if (bestLocation == null
//                    || l.getAccuracy() < bestLocation.getAccuracy()) {
//                //Log.d("found best last known location: %s", l);
//                bestLocation = l;
//            }
//        }
//        if (bestLocation == null) {
//            return null;
//        }
//        return bestLocation;
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent goback = new Intent(SponserProfile.this , MainActivity.class);
//        startActivity(goback);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    public void onTabSelected(MaterialTab tab) {

    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

//    @Override
//    public void onLocationChanged(Location location) {
//        //You had this as int. It is advised to have Lat/Loing as double.
//        double lat = location.getLatitude();
//        double lng = location.getLongitude();
//
//        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
//        StringBuilder builder = new StringBuilder();
//        try {
//            List<Address> address = geoCoder.getFromLocation(lat, lng, 1);
//            int maxLines = address.get(0).getMaxAddressLineIndex();
//            for (int i=0; i<maxLines; i++) {
//                String addressStr = address.get(0).getAddressLine(i);
//                builder.append(addressStr);
//                builder.append(" ");
//            }
//
//            String fnialAddress = builder.toString(); //This is the complete address.
//
//            //latituteField.setText(String.valueOf(lat));
//            //longitudeField.setText(String.valueOf(lng));
//            //addressField.setText(fnialAddress); //This will display the final address.
//            AppConsts.ShowToast(getApplicationContext() , fnialAddress);
//
//        } catch (IOException e) {
//            // Handle IOException
//        } catch (NullPointerException e) {
//            // Handle NullPointerException
//        }
//
//    }
//
//    @Override
//    public void onStatusChanged(String s, int i, Bundle bundle) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String s) {
//        Toast.makeText(this, "Enabled new provider " + provider,
//                Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onProviderDisabled(String s) {
//        Toast.makeText(this, "Disabled provider " + provider,
//                Toast.LENGTH_SHORT).show();
//    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {


            switch (num) {
                case 0: // Fragment # 0 - This will show image
                    return ProfileFrag.init(num);
                //return null;
                case 1: // Fragment # 1 - This will show image
                    return ScannerFrag.init(num);
                case 2:
                    return SettingsFrag.init(num);
                default:// Fragment # 2-9 - Will show list
                    //return AllCollections.init(num);
                    return null;
            }
        }

        @Override
        public int getCount() {

            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return items[position];
        }

    }
}
