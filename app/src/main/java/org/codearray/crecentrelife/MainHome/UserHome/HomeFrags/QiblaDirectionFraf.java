package org.codearray.crecentrelife.MainHome.UserHome.HomeFrags;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.codearray.crecentrelife.MainHome.UserHome.HomeFrags.HomeUtil.MySensorEventListener;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.SettingsFrag;
import org.codearray.crecentrelife.R;

import static android.content.Context.SENSOR_SERVICE;


public class QiblaDirectionFraf extends Fragment{





    SensorManager sensorManager;
    SensorEventListener eListener;
    Sensor accelerometer, magFieldSensor;
    AlertDialog dialogBuilder;
    TextView tvHeading;
    TextView tvMag;
    LinearLayout l;


    int fragNum;
    public static QiblaDirectionFraf init(int val) {
        QiblaDirectionFraf truitonList = new QiblaDirectionFraf();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragNum = getArguments() != null ? getArguments().getInt("val") : 1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_qibla_direction_fraf,
                container, false);





        l = (LinearLayout) layoutView.findViewById(R.id.Layout);
        l.setOrientation(LinearLayout.VERTICAL);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        ImageView imgCompass = new ImageView(getActivity().getApplicationContext());
        imgCompass.setImageResource(R.drawable.compasss);
        imgCompass.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imgCompass.setAdjustViewBounds(true);

        TextView tvDegree = new TextView(getActivity().getApplicationContext());
        tvDegree.setTextSize(35);
        tvDegree.setGravity(Gravity.CENTER_HORIZONTAL);
        tvDegree.setTextColor(Color.WHITE);
        tvDegree.setPaddingRelative(0,100,0,0);

        //Add views to layout
        l.addView(imgCompass);
        l.addView(tvDegree);

        tvMag = new TextView(getActivity().getApplicationContext());
        tvMag.setGravity(Gravity.CENTER_HORIZONTAL);
        tvMag.setTextColor(Color.WHITE);
        tvMag.setTextSize(18);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("enable", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("magField", false)){
            l.addView(tvMag);
        }

        //Creating senor manager
        sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

        //Getting sensors
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magFieldSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        //Creating sensor event listener
        //Register sensors with event listener
        eListener = new MySensorEventListener(sensorManager, imgCompass, tvDegree, tvMag,getActivity());
        sensorManager.registerListener(eListener, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(eListener, magFieldSensor, SensorManager.SENSOR_DELAY_GAME);


        return layoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(eListener, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(eListener, magFieldSensor, SensorManager.SENSOR_DELAY_GAME);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("enable", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("magField", false) && tvMag.getParent() == null) {
            l.addView(tvMag);
        }else if(!sharedPreferences.getBoolean("magField", false) && tvMag.getParent() != null){
            l.removeView(tvMag);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(eListener);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
