package org.codearray.crecentrelife.MainHome.UserHome.Profiles;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.OfficersFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ScannerFrag;
import org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.SettingsFrag;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

import java.util.HashMap;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class FieldOfficerProfile extends AppCompatActivity  implements MaterialTabListener {




    MaterialTabHost tabHost;
    ViewPager pager;
    ViewPagerAdapter adapter;

    String userrole;
    LoginSession loginSession;

    ImageView logbtn;
    private LocationManager locationManager;
    private String provider;

    //String items[] = {"Profile", "Settings"};
    String items[] = {"Profile", "Field Officer", "Settings"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);



        loginSession = new LoginSession(this);
        HashMap<String, String> detail = loginSession.getUserDetails();
        userrole = detail.get(LoginSession.user_role);

        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager);
        // init view pager
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);

            }
        });
        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent goback = new Intent(FieldOfficerProfile.this , MainActivity.class);
//        startActivity(goback);
        finish();
    }

    @Override
    public void onTabSelected(MaterialTab tab) {

    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {


            switch (num) {
                case 0: // Fragment # 0 - This will show image
                    return ProfileFrag.init(num);
                //return null;
                case 1: // Fragment # 1 - This will show image
                    return OfficersFrag.init(num);
                case 2:
                    return SettingsFrag.init(num);
                default:// Fragment # 2-9 - Will show list
                    //return AllCollections.init(num);
                    return null;
            }
        }

        @Override
        public int getCount() {

            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return items[position];
        }

    }
}
