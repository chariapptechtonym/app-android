package org.codearray.crecentrelife.MainHome.UserHome.HomeFrags;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.NamazTimeLibaray.PrayerTimes;
import org.codearray.crecentrelife.MainHome.NamazTimeLibaray.TimeCalculator;
import org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.PrayersType;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types.AngleCalculationType.KARACHI;


public class NamazTimeFrag extends Fragment {

    TextView _fajar;
    TextView _sunrise;
    TextView _zahur;
    TextView _assar;
    TextView _magrib;
    TextView _isha;
    Geocoder geocoder;
    SharedPreferences prefs;
    JSONObject responseObject;
    LocationManager locationManager;
    JSONArray getData;
    // View v;
    TextView nextPray;
    int fragNum;

    public static NamazTimeFrag init(int val) {
        NamazTimeFrag truitonList = new NamazTimeFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        geocoder = new Geocoder(this.getActivity().getApplicationContext(), Locale.getDefault());

        // fragNum = getArguments() != null ? getArguments().getInt("val") : 1;


    }

    private void getCurrentLocation() {
        Criteria criteria = new Criteria();
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,1);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            // return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if(location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            SharedPreferences.Editor edit = prefs.edit();
            edit.putFloat("lat", (float) latitude);
            edit.putFloat("long", (float) longitude);
            edit.commit();
            Log.e("latitude", String.valueOf(latitude));
            Log.e("longitude", String.valueOf(longitude));
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);


            } catch (IOException e) {
                e.printStackTrace();
            }
            //remove any existing marker

            if (addresses != null) {
                if (addresses.size() > 0) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("user_add_st", addresses.get(0).getSubLocality());
                    editor.putString("user_add_city", addresses.get(0).getLocality());
                    editor.putString("user_add_cont", addresses.get(0).getCountryName());
                    editor.putString("user_add_state", addresses.get(0).getAdminArea());

                    editor.commit();

                    getCurrentTimeStamp();
//
                }

            }

        }
    }
    private void getCurrentTimeStamp()
    {
        Long tsLong = System.currentTimeMillis()/1000;
        Float prevTime = prefs.getFloat("previousTime",0);
        if(prevTime != 0)
        {
            if(tsLong - prevTime >= 21600)
            {
                new GetPrayerTimeTask().execute();
            }
        }
        else
        {
            new GetPrayerTimeTask().execute();
        }

        String ts = tsLong.toString();

        Log.e("timestamp",String.valueOf(tsLong));
    }
    public class GetPrayerTimeTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(getActivity())
                    .title("Please Wait")
                    .content("Fetching Prayers time")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                String count =prefs.getString("user_add_cont","");//detail.get(LoginSession.country);
                String city =prefs.getString("user_add_city","");
                String address;
                if(city.length() != 0 && count.length()!= 0)
                {
                   address = city + "%20" + count;
                }
                else if(city.length() != 0)
                {
                    address = city;
                }
                else if(count.length()!= 0)
                {
                    address = count;
                }
                else
                {
                    address = "";
                }
//                new MaterialDialog.Builder(getActivity().getApplicationContext())
//                        .title("Please Wait")
//                        .progress(true, 0)
//                        .show();
               // pd.show();
                responseString = HttpRequests.GetPrayerTime(address);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
            {
                nDialog.dismiss();
            }
            try
            {
                if(s != null) {
                    if (s.contains("forbidden")) {

                    } else {
                        responseObject = new JSONObject(s);
                        getData = responseObject.getJSONArray("items");

                        UpdateAzanTimes(12.5, 24.5);
                    }
                }

            }catch (Exception e)
            {
                Log.e("Exception here",e.toString());
            }





        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_namaz_time,
                container, false);
        String locationProviders = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (locationProviders == null || locationProviders.equals("")) {

            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        _fajar = (TextView) layoutView.findViewById(R.id.fajar_time_id);
        _sunrise = (TextView) layoutView.findViewById(R.id.sunrise_time_id);
        _zahur = (TextView) layoutView.findViewById(R.id.zahur_time_id);
        _assar = (TextView) layoutView.findViewById(R.id.asaar_time_id);
        _magrib = (TextView) layoutView.findViewById(R.id.margib_time_id);
        _isha = (TextView) layoutView.findViewById(R.id.ishaa_time_id);
        nextPray = (TextView) layoutView.findViewById(R.id.next_pray_time_id);


        nextPray.setText("You have no new updates currently");
        //Azan();
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,1);
        locationManager = (LocationManager)
                getActivity().getSystemService(LOCATION_SERVICE);
       // new GetUserLocation().execute();
        //v = layoutView;

//        new GetPrayerTimeTask().execute();
        getCurrentLocation();
        return layoutView;
    }

    @Override public void onResume() {
        super.onResume();

        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,1);
        getCurrentLocation();

    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
//            Toast.makeText(getActivity(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }

    public void Azan() {
    }



    public class GetUserLocation  extends AsyncTask<Void, Void, Location> {

        @Override
        protected Location doInBackground(Void... voids) {
            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);
                // Log.d("last known location, provider: %s, location: %s", provider, l);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {
                    //Log.d("found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }
            return bestLocation;
        }

        @Override
        protected void onPostExecute(Location location) {
            super.onPostExecute(location);




            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();



            } else {

                nextPray.setText("Your Location is not available");

            }
        }
    }

    private void UpdateAzanTimes(double lat, double longs) {
        GregorianCalendar date = new GregorianCalendar();
        //System.out.println(date.getTimeInMillis());
        PrayerTimes prayerTimes = new TimeCalculator().date(date).location(lat, longs,
                0, 0).timeCalculationMethod(KARACHI).umElQuraRamadanAdjustment(false).calculateTimes();

        SimpleDateFormat myFormat = new SimpleDateFormat("hh:mm aa");
        //String time = myFormat.format(a);
        prayerTimes.setUseSecond(true);
        SharedPreferences.Editor editor = prefs.edit();
        //long difference = prayerTimes.getPrayTime(PrayersType.SUNRISE) - prayerTimes.getPrayTime(PrayersType.FAJR);
        try {
            JSONObject jo = getData.getJSONObject(0);

            _fajar.setText((String)  jo.get("fajr").toString());
            _zahur.setText((String)  jo.get("dhuhr").toString());
            _assar.setText((String)  jo.get("asr").toString());
            _sunrise.setText((String)  jo.get("shurooq").toString());
            _magrib.setText((String)  jo.get("maghrib").toString());
            _isha.setText((String)  jo.get("isha").toString());

            Long tsLong = System.currentTimeMillis()/1000;
            Float prevTime = prefs.getFloat("previousTime",0);
            editor.putFloat("previousTime",tsLong);
            editor.commit();
        } catch (JSONException e) {
            editor.putFloat("previousTime",0);
            e.printStackTrace();
        }
//        _fajar.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.FAJR))));
//        _sunrise.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.SUNRISE))));
//        _zahur.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.ZUHR))));
//        _assar.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.ASR))));
//        _magrib.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.MAGHRIB))));
//        _isha.setText(String.valueOf(myFormat.format(prayerTimes.getPrayTime(PrayersType.ISHA))));
    }


    private void isLocationEnabled() {


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted


                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    //new GetUserLocation().execute();

                    getCurrentLocation();

                } else {

                    // permission denied
                    Toast.makeText(getActivity(), "GPS Disabled, enable GPS and allow app to use the service. Or app will not work properly.", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
}
