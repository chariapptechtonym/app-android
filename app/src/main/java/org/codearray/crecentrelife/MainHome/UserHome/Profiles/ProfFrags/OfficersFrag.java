package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.Family.FamilyActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Family.FamilyAdpater;
import org.codearray.crecentrelife.R;
import org.codearray.crecentrelife.Values.AllValuesOfArray;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;


public class OfficersFrag extends Fragment {


    ListView mainCatListView;
    JSONArray getData;
    String[] recp_id;
    String[] recp_firstName;
    String[] recp_lastName;
    String[] gender;
    String[] recp_class;
    String[] recp_dob;
    String[] recp_city;
    String[] recp_category;


    int fragNum;
    public static OfficersFrag init(int val) {
        OfficersFrag truitonList = new OfficersFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragNum = getArguments() != null ? getArguments().getInt("val") : 1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_officers,
                container, false);
        mainCatListView = (ListView) layoutView.findViewById(R.id.mainCatListView);
        mainCatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), FieldOfficerDetailActivity.class);
                AllValuesOfArray.indexArray = i;
                AllValuesOfArray.array = getData;
                startActivity(intent);
            }
        });
        return layoutView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new LoadRecipents().execute();

    }


    public class LoadRecipents extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(getActivity())
                    .content("Please wait...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.GetRecipientsOfficers("1");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();


            if(s.length() == 0)
            {
                new MaterialDialog.Builder(getActivity())
                        .content("No Groups Found")
                        .positiveText("Ok")
                        .show();
            }
            else {
                //  Const.showToast(AppStart.this, s);
                try {
                    JSONObject jsonObj = new JSONObject(s);
                    getData = jsonObj.getJSONArray("result");

                    recp_id = new String[getData.length()];
                    recp_firstName = new String[getData.length()];
                    recp_lastName = new String[getData.length()];
                    recp_dob = new String[getData.length()];
                    recp_category = new String[getData.length()];
                    gender = new String[getData.length()];
                    recp_class = new String[getData.length()];
                    recp_city = new String[getData.length()];

                    if (getData.length() > 0) {

                        for (int i = 0; i < getData.length(); i++) {
                            JSONObject c = getData.getJSONObject(i);
                            recp_id[i] = c.getString("recp_id");
                            recp_firstName[i] = c.getString("recp_firstName");
                            recp_lastName[i] = c.getString("recp_lastName");
                            recp_dob[i] = c.getString("recp_dob");
                            gender[i] = c.getString("recp_gender");
                            recp_class[i] = c.getString("recp_class");
                            recp_category[i] = c.getString("recp_category");
                            recp_city[i] = c.getString("recp_city");
                        }

                        FamilyAdpater adapter = new FamilyAdpater(getActivity() ,recp_id, recp_firstName, recp_lastName, recp_dob, gender, recp_class, recp_category , recp_city);
                        mainCatListView.setAdapter(adapter);


                    }
                } catch (Exception e) {

                }
            }

        }
    }
}
