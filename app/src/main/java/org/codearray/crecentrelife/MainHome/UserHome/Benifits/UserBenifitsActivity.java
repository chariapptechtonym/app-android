package org.codearray.crecentrelife.MainHome.UserHome.Benifits;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.braintreepayments.api.Json;

import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class UserBenifitsActivity extends AppCompatActivity {


    TextView goback;


    TextView open_web_coups;

    String[] offer_name;
    String[] org_id;
    String[] voucher_end_date;
    String[] voucher_location;
    String[] voucher_conditions;
    String[] voucher_discount;
    String[] voucher_id;
    String[] ou_id;

    double latitude;
    double longitude;

    ListView benilist;

    LinearLayout noCoups;


    LoginSession loginSession;  // to store the user session for the auto login.
    String userId;
    LocationManager locationManager;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_benifits);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        goback = (TextView) findViewById(R.id.goBackBenifits);
        open_web_coups = (TextView) findViewById(R.id.open_web_coups);
        benilist = (ListView) findViewById(R.id.benifits_list);

        noCoups = (LinearLayout) findViewById(R.id.no_coupons);

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startBen = new Intent(UserBenifitsActivity.this, MainActivity.class);
                startActivity(startBen);
                finish();
            }
        });


        open_web_coups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://crescentrelief.org.au/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        loginSession = new LoginSession(this);  //making class
        locationManager = (LocationManager)
                getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        HashMap<String ,String> detail = loginSession.getUserDetails();
        userId =detail.get(LoginSession.userid);
        userId = prefs.getString("ou_id","");
        new GetUserLocation().execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent startBen = new Intent(UserBenifitsActivity.this, MainActivity.class);
//        startActivity(startBen);
        finish();
    }

    //get user location the important step
    public class GetUserLocation  extends AsyncTask<Void, Void, Location> {

        @SuppressLint("LongLogTag")
        @Override
        protected Location doInBackground(Void... voids) {
            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;

            for (String provider : providers) {
                @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);
                // Log.d("last known location, provider: location:");
                if (l == null) {

                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {
                    //Log.d("found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }
            return bestLocation;
        }

        @Override
        protected void onPostExecute(Location location) {
            super.onPostExecute(location);

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                new LoadBenifits().execute();
            } else {
//                latitude = 33.3;
//                longitude = 73.33;

//                new LoadBenifits().execute();
            }
        }
    }

    //get all list items
    //loading the categoires
    public class LoadBenifits extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(UserBenifitsActivity.this)
                    .content("Getting your Benifits...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.GetActiveVouchers(userId, latitude , longitude);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();


            if(!s.contains("result"))
            {
                noCoups.setVisibility(View.VISIBLE);
            }
            else {
                //  Const.showToast(AppStart.this, s);
                try {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");


                    offer_name = new String[getData.length()];
                    org_id = new String[getData.length()];
                    voucher_end_date = new String[getData.length()];
                    voucher_location = new String[getData.length()];
                    voucher_conditions = new String[getData.length()];
                    voucher_discount = new String[getData.length()];
                    voucher_id = new String[getData.length()];
                    ou_id = new String[getData.length()];

                    if (getData.length() > 0) {

                        for (int i = 0; i < getData.length(); i++) {

                            JSONObject c = (JSONObject) getData.get(i);
                            voucher_id[i] = c.getString("voucher_id");
                            org_id[i] = c.getString("org_id");
                            offer_name[i] = c.getString("offer_name");
                            voucher_end_date[i] = c.getString("voucher_end_date");
                            voucher_location[i] = c.getString("voucher_location");
                            voucher_conditions[i] = c.getString("voucher_conditions");
                            voucher_discount[i] = c.getString("voucher_discount");
                            ou_id[i] = c.getString("ou_id");

                        }


                        BenifitsAdapter adapter = new BenifitsAdapter(UserBenifitsActivity.this , offer_name, voucher_end_date, voucher_conditions, voucher_discount, voucher_id , org_id , voucher_location, ou_id);
                        benilist.setAdapter(adapter);
                       // mainCatListView.setAdapter(adapter);
                        //CategoriesMainAdpater adapter = new CategoriesMainAdpater(getActivity() , cat_id, usernsame, cat_name, cat_desc, cat_image );
                        //mainCatListView.setAdapter(adapter);

                    }
                } catch (Exception e) {
                    Log.e("Error",e.toString());
                }
            }

        }
    }
}
