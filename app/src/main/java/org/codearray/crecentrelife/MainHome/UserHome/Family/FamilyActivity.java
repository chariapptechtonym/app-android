package org.codearray.crecentrelife.MainHome.UserHome.Family;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.BenifitsAdapter;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.UserBenifitsActivity;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserHome.Projects.MainProjectsActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class FamilyActivity extends AppCompatActivity implements View.OnClickListener{



    ImageView add_fam;
    TextView goBackHome;
    AlertDialog dialog;
    String android_id;
    SharedPreferences prefs;
    ListView familyListView;
    String[] recp_id;
    String[] recp_firstName;
    String[] recp_lastName;
    String[] gender;
    String[] recp_class;
    String[] recp_dob;
    String[] recp_city;
    String[] recp_category;
    Button btnSignup,btnForgotPassword;
    JSONArray getData;
    EditText etFName,etLName,etPhoneNumber,etEmail,etPassword;
    LinearLayout nofamily;
    TextView aboutFam;

    LoginSession loginSession;  // to store the user session for the auto login.
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String ou_id = prefs.getString("ou_id","");

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        add_fam = (ImageView) findViewById(R.id.family_req_btn_id);
        goBackHome = (TextView) findViewById(R.id.goBackFamily);

        familyListView = (ListView) findViewById(R.id.family_list);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        nofamily = (LinearLayout) findViewById(R.id.no_fmily_list);

        aboutFam = (TextView) findViewById(R.id.about_btn);

        add_fam.setOnClickListener(this);
        goBackHome.setOnClickListener(this);
        aboutFam.setOnClickListener(this);

        loginSession = new LoginSession(this);  //making class

        HashMap<String ,String> detail = loginSession.getUserDetails();
        userId =detail.get(LoginSession.userid);

        familyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Item index",String.valueOf(i));
                String age,gender, city,status,act,actSecond,name;
                age = gender = city = status = act = actSecond = name ="";
                try {
                    name = getData.getJSONObject(i).get("recp_firstName").toString() +" "+getData.getJSONObject(i).get("recp_lastName").toString();
                     age = getData.getJSONObject(i).get("recp_dob").toString();
                     gender = getData.getJSONObject(i).get("recp_gender").toString();
                     city = getData.getJSONObject(i).get("recp_city").toString();
                     status = getData.getJSONObject(i).get("recp_status").toString();
                     act = "";
                     actSecond = "";//getData.getJSONObject(i).get("recp_gender").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(FamilyActivity.this,FamilyDetail.class);
                intent.putExtra("gender",gender);
                intent.putExtra("age",age);
                intent.putExtra("city",city);
                intent.putExtra("status",status);
                intent.putExtra("act",act);
                intent.putExtra("actSecond",actSecond);
                intent.putExtra("fullName",name);

                startActivity(intent);

            }
        });

        if(prefs.getString("user_ref_id","") != "") {
            new FamilyList().execute();
        }
        else
            nofamily.setVisibility(View.VISIBLE);

//        add_fam.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MaterialDialog.Builder dialog = new MaterialDialog.Builder(FamilyActivity.this)
//                        .title("Request Sent")
//                        .content("Your request to add a Family Member has been sent to Admin")
//                        .positiveText("OK");
//                dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        //// TODO
//                        AppConsts.ShowToast(getApplicationContext() , "Done");
//
//                    }
//                });
//                dialog.show();
//            }
//        });


    }

    AlertDialog dialogBuilder;
    private void calibrateDialog(){
        LayoutInflater layoutInflater = LayoutInflater.from(FamilyActivity.this);
        View calibrateView = layoutInflater.inflate(R.layout.family_view_example, null);
        dialogBuilder = new AlertDialog.Builder(FamilyActivity.this)
                .setView(calibrateView)
                .setTitle("About Family")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .create();

        dialogBuilder.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent closeNet = new Intent(FamilyActivity.this, MainActivity.class);
//        startActivity(closeNet);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.family_req_btn_id:
                String check = prefs.getString("user_ref_id","");
                if(prefs.getString("user_ref_id","") != "") {
                    Intent family = new Intent(FamilyActivity.this, MainProjectsActivity.class);
                    family.putExtra("family", "family");
                    startActivity(family);
                    finish();
                }
                else
                {
                    showLogin();
                }
                break;

            case R.id.goBackFamily:
                Intent closeNet = new Intent(FamilyActivity.this, MainActivity.class);
                startActivity(closeNet);
                finish();
                break;

            case R.id.about_btn:
                calibrateDialog();
                break;
        }
    }
    private void showLogin()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_login_popup, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        btnSignup = (Button)alertLayout.findViewById(R.id.btnSignup);
        btnForgotPassword = (Button)alertLayout.findViewById(R.id.btnForgotPassword);
        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
        etPassword = (EditText)alertLayout.findViewById(R.id.editPassword);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();
        final AlertDialog currentDialog =dialog;
        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etPassword.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new LoginTask().execute();
                }
//                else
//                {
//                    new UpdateInfoTask().execute();
//
//                }

            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSignup();
                Log.e("button","Signup pressed");

            }
        });
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForgotPassword();
                Log.e("button","forgot pressed");
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentDialog.dismiss();
            }
        });
    }

    public class LoginTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.UserLogin(etEmail.getText().toString(),etPassword.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                if(s.contains("forbidden"))
                {
                    JSONObject jsonObj = new JSONObject(s);
                    String msg = jsonObj.getString("msg").toString();
                    AlertDialog alertDialog = new AlertDialog.Builder(FamilyActivity.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");
                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
                    for (int i = 0; i < getData.length(); i++) {
                        JSONObject c = getData.getJSONObject(i);
                        String user_id = c.getString("user_id");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user_id",user_id);
                        String user_ref_id = c.getString("user_ref_id");
                        editor.putString("user_ref_id",user_ref_id);
                        String user_first_name = c.getString("user_first_name");
                        editor.putString("user_first_name",user_first_name);
                        String user_last_name = c.getString("user_last_name");
                        editor.putString("user_last_name",user_last_name);
                        String user_mobile = c.getString("user_mobile");
                        String user_email = c.getString("user_email");
                        editor.putString("user_email",user_email);
                        editor.putString("user_mobile",user_mobile);
                        String country = c.getString("user_add_cont");
                        editor.putString("user_add_cont",country);
                        String state = c.getString("user_add_state");
                        editor.putString("user_add_state",state);
                        String city = c.getString("user_add_city");
                        editor.putString("user_add_city",city);
                        String street = c.getString("user_add_st");
                        editor.putString("user_add_st",street);
                        String ou_id = c.getString("ou_id");
                        editor.putString("ou_id",ou_id);
                        String user_role = c.getString("role_name");
                        editor.putString("role_name",user_role);
                        editor.putBoolean("isUserLoggedIn",true);
                        editor.commit();

                        Log.d("userdetais  ", user_id + "-------" + user_first_name + "-----" + ou_id);

                        nofamily.setVisibility(View.INVISIBLE);

                        new FamilyList().execute();


                        // AppConsts.ShowToast(getApplicationContext() , user_id + "-------" + ou_id + "-------" + user_first_name + "-------" + user_last_name + "-------" + user_mobile+ "-------" + country + "-------" + state + "-------" + city + "-------" + street+ "-------" + usernameInput+ "-------" + user_role );
                    }
                    FamilyActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });

                }

            }catch (Exception e)
            {
                Log.e("Exception Here",e.toString());
            }





        }
    }
    public void showSignup()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.information_alert_dialog, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        etFName = (EditText)alertLayout.findViewById(R.id.editFirstName);
        etLName = (EditText)alertLayout.findViewById(R.id.editLastName);
        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
        etPhoneNumber = (EditText)alertLayout.findViewById(R.id.editPhoneNumber);
        etPassword = (EditText)alertLayout.findViewById(R.id.editPassword);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();

        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etFName.getText().length() == 0 || etPhoneNumber.getText().length() == 0 || etLName.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new UpdateInfoTask().execute();

                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
    private void showPopup()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.information_alert_dialog, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        etFName = (EditText)alertLayout.findViewById(R.id.editFirstName);
        etLName = (EditText)alertLayout.findViewById(R.id.editLastName);
        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);
        etPhoneNumber = (EditText)alertLayout.findViewById(R.id.editPhoneNumber);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();

        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etFName.getText().length() == 0 || etPhoneNumber.getText().length() == 0 || etLName.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new UpdateInfoTask().execute();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public class UpdateInfoTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.registerNewUser("",android_id,etFName.getText().toString(),etLName.getText().toString(),etPhoneNumber.getText().toString(),"",etEmail.getText().toString(),"","","","","",etPassword.getText().toString(),"1");
//                responseString = HttpRequests.UpdateInfo(android_id,etFName.getText().toString(),etLName.getText().toString(),etEmail.getText().toString(),etPhoneNumber.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                JSONObject jsonObj = new JSONObject(s);
                String msg = jsonObj.getString("msg").toString();
                if(s.contains("forbidden"))
                {

                    AlertDialog alertDialog = new AlertDialog.Builder(FamilyActivity.this).create();
                    alertDialog.setTitle("Success");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {

                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();

//                    JSONObject jsonObj = new JSONObject(s);
//
//                    JSONObject getData = jsonObj.getJSONObject("user");
//
////                    JSONArray getData = jsonObj.getJSONArray("user");
//                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
//
////                        JSONObject c = getData.getJSONObject(i);
//                        int user_id = (int) getData.get("user_id");
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("user_id",user_id+"");
//                        String user_ref_id = (String) getData.get("user_ref_id").toString();
//                        editor.putString("user_ref_id",user_ref_id);
//                        String user_first_name =(String)  getData.get("user_first_name").toString();
//                        editor.putString("user_first_name",user_first_name);
//                        String user_last_name =(String)  getData.get("user_last_name").toString();
//                        editor.putString("user_last_name",user_last_name);
//                        String user_email = (String) getData.get("user_email").toString();
//                        editor.putString("user_email",user_email);
//                        String user_mobile =(String)  getData.get("user_mobile").toString();
//                        editor.putString("user_mobile",user_mobile);
//                        String country =(String)  getData.get("user_add_cont").toString();
////                        editor.putString("user_add_cont",country);
//                        String state = (String) getData.get("user_add_state").toString();
////                        editor.putString("user_add_state",state);
//                        String city = (String) getData.get("user_add_city").toString();
////                        editor.putString("user_add_city",city);
//                        String street =(String)  getData.get("user_add_st").toString();
////                        editor.putString("user_add_st",street);
//                        int ou_id = (int) getData.get("ou_id");
//                        editor.putString("ou_id",ou_id+"");
//                        String user_role =(String)  getData.get("role_name").toString();
//                        editor.putString("role_name",user_role);
//                        editor.putBoolean("isUserLoggedIn",true);
//                        editor.commit();



                    FamilyActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                }

            }catch (Exception e)
            {
                Log.e("exception","exception occured");
                Toast.makeText(getApplicationContext(),e.getMessage().toString(),Toast.LENGTH_SHORT).show();
            }





        }
    }
    public class FamilyList extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(FamilyActivity.this)
                    .content("Getting your Family Members...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                userId = prefs.getString("ou_id","");
                responseString = HttpRequests.GetFamilyMemebers(userId);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();

            if(s!= null) {
                if (!s.contains("result")) {
//                new MaterialDialog.Builder(FamilyActivity.this)
//                        .content("No Family Found")
//                        .positiveText("Ok")
//                        .show();
                    nofamily.setVisibility(View.VISIBLE);
                } else {
                    //  Const.showToast(AppStart.this, s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        getData = jsonObj.getJSONArray("result");
                        recp_id = new String[getData.length()];
                        recp_firstName = new String[getData.length()];
                        recp_lastName = new String[getData.length()];
                        recp_dob = new String[getData.length()];
                        recp_category = new String[getData.length()];
                        gender = new String[getData.length()];
                        recp_class = new String[getData.length()];
                        recp_city = new String[getData.length()];

                        if (getData.length() > 0) {

                            for (int i = 0; i < getData.length(); i++) {
                                JSONObject c = getData.getJSONObject(i);
                                recp_id[i] = c.getString("recp_id");
                                recp_firstName[i] = c.getString("recp_firstName");
                                recp_lastName[i] = c.getString("recp_lastName");
                                recp_dob[i] = c.getString("recp_dob");
                                gender[i] = c.getString("recp_gender");
                                recp_class[i] = c.getString("recp_class");
                                recp_category[i] = c.getString("recp_category");
                                recp_city[i] = c.getString("recp_city");
                            }


                            FamilyAdpater adapter = new FamilyAdpater(FamilyActivity.this, recp_id, recp_firstName, recp_lastName, recp_dob, gender, recp_class, recp_category, recp_city);
                            familyListView.setAdapter(adapter);


                        }
                    } catch (Exception e) {

                    }
                }
            }
        }
    }

    public void showForgotPassword()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_forgot_password_popup, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);


        etEmail = (EditText)alertLayout.findViewById(R.id.editEmail);

        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        ImageButton btnSubmit = (ImageButton) alertLayout.findViewById(R.id.btnSubmitNow);
        Button cancel = (Button)alertLayout.findViewById(R.id.btnCancel) ;
        dialog = alert.create();

        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Success","Ok now");
                if(etEmail.getText().length()==0 || etPassword.getText().length() == 0 )
                {
                    Log.e("Error","Pls fill the required fields");
                }
                else
                {
                    new ForgotPasswordTask().execute();
                }
//                else
//                {
//                    new UpdateInfoTask().execute();
//
//                }

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public class ForgotPasswordTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {

                responseString = HttpRequests.ForgotPassword(etEmail.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try
            {
                JSONObject jsonObj = new JSONObject(s);
                String msg = jsonObj.getString("msg").toString();
                if(s.contains("forbidden"))
                {

                    AlertDialog alertDialog = new AlertDialog.Builder(FamilyActivity.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(msg);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {
                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    FamilyActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                }

            }catch (Exception e)
            {
                Log.e("Exception Here",e.toString());
            }





        }
    }
}
