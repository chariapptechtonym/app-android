package org.codearray.crecentrelife.MainHome.UserHome.Projects;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

import java.io.IOException;
import java.util.HashMap;

public class ProjectReciptActivity extends AppCompatActivity {


    String programname , amount , isfamily , programid;
    TextView _prgramname , _amount , uname , urefid;
    ImageView btnhome;
    boolean requet = false;
    SharedPreferences prefs;
    String name , id;

    LoginSession session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_recipt);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        _prgramname = (TextView) findViewById(R.id.recp_project_title);
        _amount = (TextView) findViewById(R.id.amount_paid_rcp);

        urefid = (TextView) findViewById(R.id.recipt_user_id);
        uname = (TextView) findViewById(R.id.recipt_user_name);

        btnhome = (ImageView) findViewById(R.id.home_btn);

        Intent getDate = getIntent();

        programname = getDate.getStringExtra("projectname");
        amount = getDate.getStringExtra("amount");
        isfamily = getDate.getStringExtra("isfamily");
        programid = getDate.getStringExtra("programid");

        session = new LoginSession(this);
        HashMap<String ,String> detail = session.getUserDetails();
        id =prefs.getString("user_ref_id","");
        name = prefs.getString("user_first_name","");

        urefid.setText(id);
        uname.setText(name);


        if(isfamily.contains("yes")){
            new RequestMemberTask().execute();
        }else{

        }






        _prgramname.setText(programname);
        _amount.setText(amount);


        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    //AppConsts.ShowToast(getApplicationContext() , "thats not family:  "+ programid);
//                    Intent home = new Intent(ProjectReciptActivity.this , MainActivity.class);
//                    startActivity(home);
                    finish();


            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if(!requet){
//          //  AppConsts.ShowToast(getApplicationContext() , "");
//            MaterialDialog.Builder dialog = new MaterialDialog.Builder(ProjectReciptActivity.this)
//                    .title("Warning...!!")
//                    .content("Your request has not been yet submitted. Please make sure you submit your request by pressing the submit button")
//                    .positiveText("OK");
//            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
//                @Override
//                public void onClick(MaterialDialog dialog, DialogAction which) {
//                    //// TODO
//                  //  AppConsts.ShowToast(getApplicationContext() , "Done");
//
//                    new RequestMemberTask().execute();
//                }
//            });
//            dialog.show();
//        }else{
//            Intent closeNet = new Intent(ProjectReciptActivity.this, MainActivity.class);
//            startActivity(closeNet);
//            finish();
//        }
    }

    //request a family member...!!
    public class RequestMemberTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(ProjectReciptActivity.this)
                    .title("Please Wait")
                    .content("Submitting your Request")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.RequestFamilyMember(amount , "1");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();




//            MaterialDialog.Builder dialog = new MaterialDialog.Builder(ProjectReciptActivity.this)
//                    .title("Account Registered")
//                    .content("Your Request has been sent to Admin for Review. We will contact you shortly.")
//                    .positiveText("OK");
//            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
//                @Override
//                public void onClick(MaterialDialog dialog, DialogAction which) {
//                    //// TODO
//                   // AppConsts.ShowToast(getApplicationContext() , "Done");
//                    Intent closeNet = new Intent(ProjectReciptActivity.this, MainActivity.class);
//                    startActivity(closeNet);
//                    finish();
//                }
//            });
//            dialog.show();





        }
    }

}
