package org.codearray.crecentrelife.MainHome.MyNetworks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import org.codearray.crecentrelife.R;

/**
 * Created by Haseeb on 4/14/18.
 */

public class NetworkAdptater extends ArrayAdapter<String> {

    String[] phone;
    String[] email;
    String[] firstname;
    private final Activity context;


    private MaterialDialog nDialog;



    public NetworkAdptater(Activity context, String[] phone, String[] email, String[] firstname) {
        super(context, R.layout.network_people_view, phone);
        this.context = context;
        this.phone = phone;
        this.email = email;
        this.firstname = firstname;
    }

    public class LoadViews
    {
        TextView name;
        TextView stats;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LoadViews viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.network_people_view, null);
            viewHolder = new LoadViews();
            viewHolder.name = (TextView) convertView.findViewById(R.id.net_friend_name);
            viewHolder.stats = (TextView) convertView.findViewById(R.id.show_current_status);

            convertView.setTag(viewHolder);
        }else
        {
            viewHolder = (LoadViews) convertView.getTag();
        }
        viewHolder.name.setText(firstname[position]);
        viewHolder.stats.setText("Invitation Sent.");


        return  convertView;
    }



}
