package org.codearray.crecentrelife.MainHome.UserHome.Projects;

/**
 * Created by Haseeb on 4/9/18.
 */

public class ProjectDetail {

    private String project_id;
    private String project_name;
    private String project_brief;

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_brief() {
        return project_brief;
    }

    public void setProject_brief(String project_brief) {
        this.project_brief = project_brief;
    }
}
