package org.codearray.crecentrelife.MainHome.Requests;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

import okhttp3.MultipartBody;

/**
 * Created by Haseeb on 4/9/18.
 */

public class HttpRequests {

    public static String baseURL = "http://api.chariapp.com/api";
    public static String UserRegistration(String _firstname, String _lastname, String _email, String _password, String _phone, String refid) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"user_ref_id\" : \""+refid+"\",\n  \"registrar_id\" : \"0\",\n  \"user_first_name\" : \""+_firstname+"\",\n  \"user_last_name\" : \""+_lastname+"\",\n  \"user_mobile\" : \""+_phone+"\",\n  \"user_email\" : \""+_email+"\",\n  \"user_add_cont\" : \"\",\n  \"user_add_state\" : \"\",\n  \"user_add_post_code\" : \"\",\n  \"user_add_city\" : \"\",\n  \"user_add_st\" : \"\",\n  \"user_password\" : \""+_password+"\",\n  \"org_id\" : 1\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/register")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "3a26ba74-06e7-41e8-9af1-ec90d471e021")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    public static String registerNewUser(String user_ref_id,String registrar_id,String user_first_name, String user_last_name, String user_mobile, String user_home_phone,String user_email, String user_add_cont,String user_add_state,String user_add_post_code,String user_add_city, String user_add_st,String user_password,String org_id) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("user_ref_id",user_ref_id)
                .addFormDataPart("registrar_id",registrar_id)
                .addFormDataPart("user_first_name",user_first_name)
                .addFormDataPart("user_last_name",user_last_name)
                .addFormDataPart("user_mobile",user_mobile)
                .addFormDataPart("user_home_phone",user_home_phone)
                .addFormDataPart("user_email",user_email)
                .addFormDataPart("user_add_cont",user_add_cont)
                .addFormDataPart("user_add_state",user_add_state)
                .addFormDataPart("user_add_post_code",user_add_post_code)
                .addFormDataPart("user_home_phone",user_home_phone)
                .addFormDataPart("user_add_city",user_add_city)
                .addFormDataPart("user_add_st",user_add_st)
                .addFormDataPart("user_password",user_password)
                .addFormDataPart("org_id",org_id)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/register")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    public  static String sendDataToServer(String amount , String nonce, String ou_id, String project_id) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("amount",amount)
                .addFormDataPart("nonce",nonce)
                .addFormDataPart("ou_id",ou_id)
                .addFormDataPart("project_id",project_id)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url("http://payments.chariapp.com/payments/mycheckout.php")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    public static String ForgotPassword(String email) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("user_email",email)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/password_request")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    public  static String UserLogin(String _email , String _password) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"email\" : \""+_email+"\",\n  \"password\" : \""+_password+"\"\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/login")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "42c04527-f33d-4d77-a56b-1cdea96f2bad")
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }

    public static String UpdateInfo(String deviceId, String firstName, String lastName, String email, String phoneNumber) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("device_id",deviceId)
                .addFormDataPart("first_name",firstName)
                .addFormDataPart("last_name",lastName)
                .addFormDataPart("email",email)
                .addFormDataPart("phone",phoneNumber)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/updateinfo")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String GetPrayerTime(String address) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
       String url = "http://muslimsalat.com/"+address+"/daily.json?key=3db85bdcbcd1a5caebfb584a79eaf317";
        //RequestBody body = RequestBody.create(mediaType, "{\n  \"user_ref_id\" : \"qwer123\",\n  \"registrar_id\" : \"356\",\n  \"user_first_name\" : \"Osama\",\n  \"user_last_name\" : \"AHmed\",\n  \"user_mobile\" : \"987654331\",\n  \"user_email\" : \"mapoik.tech35@gmail.com\",\n  \"user_add_cont\" : \"Pakistan\",\n  \"user_add_state\" : \"Punjab\",\n  \"user_add_post_code\" : \"\",\n  \"user_add_city\" : \"Wah Cantt\",\n  \"user_add_st\" : \"\",\n  \"user_password\" : \"1234\",\n  \"org_id\" : 1\n}");
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }

    public static String DoSilentLogin(String deviceId) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("device_id",deviceId)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/accountlogin")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String DoSilentRegister(String deviceId , String refId) throws IOException
    {
        RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                .addFormDataPart("device_id",deviceId)
                .addFormDataPart("refid",refId)
                .build();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/octet-stream");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/accountregister")
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String GetActiveProjects(String _orgId) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
        //RequestBody body = RequestBody.create(mediaType, "{\n  \"user_ref_id\" : \"qwer123\",\n  \"registrar_id\" : \"356\",\n  \"user_first_name\" : \"Osama\",\n  \"user_last_name\" : \"AHmed\",\n  \"user_mobile\" : \"987654331\",\n  \"user_email\" : \"mapoik.tech35@gmail.com\",\n  \"user_add_cont\" : \"Pakistan\",\n  \"user_add_state\" : \"Punjab\",\n  \"user_add_post_code\" : \"\",\n  \"user_add_city\" : \"Wah Cantt\",\n  \"user_add_st\" : \"\",\n  \"user_password\" : \"1234\",\n  \"org_id\" : 1\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/projects/active/"+_orgId)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "2c5fd2d7-9b5a-44df-981f-cfa75ae2a57e")
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }


    public static String GetActiveVouchers(String ouid , double lat , double lng) throws IOException
    {

        OkHttpClient client = new OkHttpClient();
            String urlOld = baseURL+"/public/benifits/usercoupons/1/1/1/"+ouid+"/"+lat+"/"+lng;
            String urlNew = baseURL+"/public/benifits/get/uservouchers/"+ouid +"/1/1";
//            https://api.chariapp.com/api/public/benifits/get/uservouchers/78/1/1
        Request request = new Request.Builder()
                .url(urlOld)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "d1118181-e01f-4f4b-a8e1-409c74ebd240")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    public static String RequestFamilyMember(String amount, String ou_id) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"amount_donated\" : \""+amount+"\",\n  \"ou_id\" : \""+ou_id+"\"\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/recp/request")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "1e7cdbad-031a-4b57-ac48-dad1fb09b6d0")
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }

    public static String GetFamilyMemebers(String ouid) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseURL+"/public/family/get/"+ouid+"/1")
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "988ad94b-eeb5-4a89-a0ad-261806f2293e")
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }

    public static String SubmitFeedback(String ouid , String feedback) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"ou_id\" : \""+ouid+"\",\n  \"feedback_text\" : \""+feedback+"\"\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/users/feedback")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "a12b365d-575f-4b70-bb33-26c2e52c3576")
                .build();

        Response response = client.newCall(request).execute();
        return  response.body().string();
    }

    public static String AddNetwork (String ouid , String feed) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, feed);
        Request request = new Request.Builder()
                .url(baseURL+"/public/network/create/"+ouid)
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "f0925f40-c26b-4ab7-bc2e-33909d2a7f25")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String GetNetwork (String orgid, String ouid) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseURL+"/public/network/get/"+orgid+"/"+ouid)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "f800ff76-ae17-4695-a1b8-0ad9059676ad")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String GetRecipientsOfficers(String orgid) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseURL+"/public/recp/get/"+orgid)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "bb65a04f-d1f1-4ac9-b491-2ec6c746f3ee")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    public static String GetCountriesList() throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://battuta.medunes.net/api/country/all/?key=96b98de9d523f3896af755c9cf6f4fe4")
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "0413fec1-6858-4d04-8aeb-4d6fbd0a578a")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String GetRegions(String region) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://battuta.medunes.net/api/region/"+region+"/all/?key=96b98de9d523f3896af755c9cf6f4fe4")
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "d02d5494-8493-497a-a3de-58f03ebab4e3")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String GetCities(String count, String reg) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        String country = URLEncoder.encode(count , "utf-8");
        String region = URLEncoder.encode(reg, "utf-8");
         String myurl = "https://battuta.medunes.net/api/city/"+country+"/search/?region="+region+"&key=96b98de9d523f3896af755c9cf6f4fe4";

        Request request = new Request.Builder()
                .url(myurl)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "ffa87aa9-d4c1-4e0a-87f7-65e83a6072de")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public  static String UpdateUserAdd(String userid, String count, String state,  String city, String street) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        String newcount = URLEncoder.encode(count , "utf-8");
        String newregi = URLEncoder.encode(state, "utf-8");
        String newcity = URLEncoder.encode(city, "utf-8");
        String newstreet = URLEncoder.encode(street, "utf-8");

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n\t\"userid\" : \""+userid+"\",\n\t\"count\" : \""+newcount+"\",\n\t\"state\" : \""+newregi+"\",\n\t\"city\" : \""+newcity+"\",\n\t\"street\" : \""+newstreet+"\"\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/user/updateadd")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "a5248e34-8052-4852-bfc6-345639f9a33b")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String UploadActivity(String recp_id , String act_details , String filePath, String contetnType , File image) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

       // MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
       // RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"recp_id\"\r\n\r\n"+recp_id+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"actname\"\r\n\r\nNo Activity\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"actdetail\"\r\n\r\n"+act_details+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"uploads\"; filename=\""+filePath+"\"\r\nContent-Type: "+contetnType+"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");

        RequestBody body = new MultipartBuilder().type(MultipartBuilder.FORM)     //setType(MultipartBody.FORM)
                .addFormDataPart("recp_id" , recp_id)
                .addFormDataPart("actname" , "None")
                .addFormDataPart("actdetail" , act_details)
                .addFormDataPart("uploads", filePath, RequestBody.create(MediaType.parse(contetnType), image))
                .build();



//        MediaType MEDIA_TYPE = MediaType.parse(contetnType); // e.g. "image/png"
//        RequestBody body = new MultipartBuilder()
//                .type(MultipartBuilder.FORM)
//                .addFormDataPart("recp_id", recp_id)
//                .addFormDataPart("actname", "None")
//                .addFormDataPart("actdetail", act_details)
//               // .addFormDataPart("filename", title + "." + imageFormat) //e.g. title.png --> imageFormat = png
//                .addFormDataPart("uploads", "anyfilename", RequestBody.create(MEDIA_TYPE, image))
//                .build();


        Request request = new Request.Builder()
                .url(baseURL+"/public/recp/createactivity")
                .post(body)
                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "2e256c32-90ad-4726-9943-3b09745a69cb")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String GetRecpActivites(String recp_id) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(baseURL+"/public/recp/get/activity/"+recp_id)
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "a69b184e-1cb2-4c03-85ec-d7e89d6a7179")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String RedeemVoucher(String vid , String ouod , String orgid) throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"voucher_id\" : \""+vid+"\",\n   \"ou_id\"  : \""+ouod+"\",\n   \"org_id\"  : \""+orgid+"\"\n}");
        Request request = new Request.Builder()
                .url(baseURL+"/public/benifits/applycode")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "1d008be3-e2d1-4340-bdd7-17a0c366325d")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}

