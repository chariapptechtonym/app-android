package org.codearray.crecentrelife.MainHome.MyNetworks;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Haseeb on 4/14/18.
 */

public class Contacts implements Parcelable  {

    public String id,name,phone,label, email;

    Contacts(String id, String name,String phone,String label, String email){
        this.id=id;
        this.name=name;
        this.phone=phone;
        this.label=label;
        this.email = email;
    }

    protected Contacts(Parcel in) {
        id = in.readString();
        name = in.readString();
        phone = in.readString();
        label = in.readString();
        email = in.readString();
    }

    public static final Creator<Contacts> CREATOR = new Creator<Contacts>() {
        @Override
        public Contacts createFromParcel(Parcel in) {
            return new Contacts(in);
        }

        @Override
        public Contacts[] newArray(int size) {
            return new Contacts[size];
        }
    };

    @Override
    public String toString()
    {
        return name+" | "+label+" : "+phone+" : "+email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(label);
        dest.writeString(email);
    }
}
