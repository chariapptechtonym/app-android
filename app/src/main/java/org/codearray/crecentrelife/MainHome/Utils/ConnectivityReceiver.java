package org.codearray.crecentrelife.MainHome.Utils;

/**
 * Created by Haseeb on 4/9/18.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;


public class ConnectivityReceiver{

    private Activity activity;


    private ConnectivityManager connectivityManager;
    private int networkType;
    private String networkTypeName;


    private TelephonyManager telephonyManager;
    private int mobileNetworkType;
    private String mobileNetworkTypeName;
    private String networkOperatorName;
    private String deviceID;

    private boolean networkStatus;

    public ConnectivityReceiver(Activity activity){
        this.activity = activity;
        System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
        setup();



    }

    //initializes variables
    private void setup() {
        networkType = -1;
        mobileNetworkType = -1;

        networkTypeName = null;
        mobileNetworkTypeName = null;
        networkOperatorName = null;

        networkStatus = false;
        deviceID = null;

        connectivityManager = ( ConnectivityManager ) activity.getSystemService( Context.CONNECTIVITY_SERVICE );
        telephonyManager = ( TelephonyManager ) activity.getSystemService( Context.TELEPHONY_SERVICE );
    }


    public boolean isConnected() {
        try {
            return connectivityManager.getActiveNetworkInfo().isConnected();
        }
        catch (NullPointerException npe) {
            return false;
        }
    }

    public int getNetworkType() {
        if (networkType != -1) {
            return networkType;
        }
        else {
            if (isConnected()) {
                networkType = connectivityManager.getActiveNetworkInfo().getType();
            }
            else {
                networkType = -1;
            }
        }
        return networkType;
    }

    //Returns network type name String ("WIFI" OR "MOBILE")
    //if not connected, return null
    public String getNetworkTypeName() {
        System.out.println("A bay bay");
        if (networkTypeName != null) {
            return networkTypeName;
        }
        else {
            System.out.println("Hey");
            if (isConnected()) {
                networkTypeName = connectivityManager.getActiveNetworkInfo().getTypeName();
            }
            else {
                networkTypeName = null;
            }
        }
        return networkTypeName;
    }


    public int getMobileNetworkType() {
        if (isConnected()) {
            if (mobileNetworkType == -1) {
                mobileNetworkType = telephonyManager.getNetworkType();
            }
        }
        else {
            mobileNetworkType = -1;
        }
        return mobileNetworkType;
    }



    public String getMobileNetworkTypeName() {
        getNetworkType();
        if (isConnected() && networkType == ConnectivityManager.TYPE_MOBILE ) {
            if (mobileNetworkTypeName == null) {
                getMobileNetworkType();

                switch (mobileNetworkType) {
                    case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                        mobileNetworkTypeName = "UNKNOWN";
                        break;
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                        mobileNetworkTypeName = "GPRS";
                        break;
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                        mobileNetworkTypeName = "EDGE";
                        break;
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                        mobileNetworkTypeName = "UMTS";
                        break;
                    default:
                        mobileNetworkTypeName = "MOBILE";
                }
            }
        }
        else {
            mobileNetworkTypeName = null;
        }
        return mobileNetworkTypeName;
    }

    //returns carrier type
    public String getNetworkOperator() {
        if (networkOperatorName == null) {
            networkOperatorName = telephonyManager.getNetworkOperatorName();
        }
        return networkOperatorName;
    }

    public boolean getNetworkStatus() {
        return networkStatus;
    }

    public void setNetworkStatus (boolean status) {
        networkStatus = status;
    }

    @SuppressLint("MissingPermission")
    public String getDeviceID () {
        if (deviceID == null) {
            deviceID = telephonyManager.getDeviceId();
        }
        return deviceID;
    }

    public void detectInternetConnection () {
        if (!isConnected()) {
            System.out.println("NOT CONNECTED");
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder
                    .setMessage("No Internet Connection Detected! Please fix and try again.")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    activity.finish();
                                }
                            }
                    )
                    .setTitle("Internet Connection Problem")
                    .show();

        }
        else {
            System.out.println("Internet Connection Detected");
        }
    }

}
