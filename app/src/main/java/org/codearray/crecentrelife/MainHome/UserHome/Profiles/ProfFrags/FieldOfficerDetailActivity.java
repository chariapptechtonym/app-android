package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.codearray.crecentrelife.CaptureImage;
import org.codearray.crecentrelife.CropUtil;
import org.codearray.crecentrelife.R;
import org.codearray.crecentrelife.UploadImageUtility;
import org.codearray.crecentrelife.Values.AllValuesOfArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//import org.codearray.crecentrelife.R;

public class FieldOfficerDetailActivity extends AppCompatActivity {
    TextView txtViewName,txtViewAge,txtViewGender,txtViewCity,txtViewStatus;
    ImageView imgViewProfile,imgViewDescription;
            ImageButton btnUpdate;
            EditText editTextDescription,editTextDate;
    CaptureImage captureImage;
    File file1;
    public Uri fileUri1;
    byte[] byteArray1;
    Bitmap newbitmap1;
    final static int PERMISSIONS_CAMERA = 3;
    final static int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 25;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_officer_detail);
        txtViewName = findViewById(R.id.textViewName);
        txtViewAge = (TextView)findViewById(R.id.textViewAge);
        txtViewCity = (TextView)findViewById(R.id.textViewCity);
        txtViewGender = findViewById(R.id.textViewGender);
        txtViewStatus = findViewById(R.id.textViewStatus);
        imgViewProfile = findViewById(R.id.imageViewProfile);
        imgViewDescription = findViewById(R.id.imageViewDescription);
        btnUpdate = findViewById(R.id.imgBtnUpdate);
        editTextDate = findViewById(R.id.editTextDate);
        editTextDescription = findViewById(R.id.editTextDescription);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Button","Clicked");

            }
        });
        imgViewDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pictureDialog();
            }
        });
        try {
            JSONObject c = AllValuesOfArray.array.getJSONObject(AllValuesOfArray.indexArray);
            txtViewName.setText(c.getString("recp_firstName")+" "+c.getString("recp_lastName"));
            txtViewAge.setText(c.getString("recp_dob"));
            txtViewStatus.setText(c.getString("recp_status"));
            txtViewGender.setText(c.getString("recp_gender"));
            txtViewCity.setText(c.getString("recp_city"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





    private void pictureDialog() {
        ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                android.Manifest.permission.CAMERA);
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                    android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    ) {
                requestPermissions(
                        new String[]{android.Manifest.permission.CAMERA},
                        PERMISSIONS_CAMERA);
                return;
            } else if ((ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    ) {

                requestPermissions(
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                return;
            } else {
                selectImage();
            }
        } else {
            selectImage();
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery",
                "Cancel"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                        captureImage = UploadImageUtility.genarateUri();
                        fileUri1 = captureImage.getUri();
                        file1 = captureImage.getFile();
                        if (fileUri1 != null) {
                            Intent intent = new Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri1);
                            startActivityForResult(intent, 1);


                        }

                } else if (options[item].equals("Choose from Gallery")) {

                        String state = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            file1 = new File(Environment
                                    .getExternalStorageDirectory(),
                                    UploadImageUtility.genarateFileName());
                        } else {
                            file1 = new File(getFilesDir(), UploadImageUtility
                                    .genarateFileName());
                        }

                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent, 2);


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == FieldOfficerDetailActivity.RESULT_OK) {
            // btnSubmitInquiry.setEnabled(true);
            if (requestCode == 1) {
                try {
                        if (fileUri1 != null) {
//                            String state = Environment.getExternalStorageState();
//                            if (Environment.MEDIA_MOUNTED.equals(state)) {
//                                file1 = new File(Environment
//                                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
//                                        , fileUri1.getPath());
//                            } else {
//                                file1 = new File(getFilesDir(), fileUri1.getPath());
//                            }
//                        filePath1 = file1.getName();
//                        fullPath1 = file1.getAbsolutePath();
//
//                        doScanFile(filePath1);


                            newbitmap1 = fixRotation(fileUri1);

//                        File cropFilePath = new File(UploadImageUtility
//                                .genarateUri().getPath());
//                        filePath = cropFilePath.getName();
//                        fullPath = cropFilePath.getAbsolutePath();
//                        newbitmap = BitmapFactory.decodeFile(file.getPath());


//                        ExifInterface ei = new ExifInterface(file.getAbsolutePath());
//                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                                ExifInterface.ORIENTATION_UNDEFINED);
//
//                        switch (orientation) {
//                            case ExifInterface.ORIENTATION_ROTATE_90:
//                                newbitmap = rotateImage(newbitmap, 90);
//                                break;
//                            case ExifInterface.ORIENTATION_ROTATE_180:
//                                newbitmap = rotateImage(newbitmap, 180);
//                                break;
//                            case ExifInterface.ORIENTATION_ROTATE_270:
//                                newbitmap = rotateImage(newbitmap, 270);
//                                break;
//                            case ExifInterface.ORIENTATION_NORMAL:
//                            default:
//                                break;
//                        }

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            newbitmap1.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                            byteArray1 = baos.toByteArray();

                            imgViewDescription.setImageBitmap(newbitmap1);

                        }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == 2) {

                try {

                        InputStream inputStream = this.getContentResolver()
                                .openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(
                                file1);
                        copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                        newbitmap1 = fixRotation(data.getData());
//                    newbitmap = BitmapFactory.decodeFile(file.getPath());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        newbitmap1.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                        byteArray1 = baos.toByteArray();

//                    File cropFilePath = new File(UploadImageUtility
//                            .genarateUri().getPath());
//                    filePath1 = cropFilePath.getName();
//                    fullPath1 = cropFilePath.getAbsolutePath();

                        imgViewDescription.setImageBitmap(newbitmap1);


                } catch (Exception e) {
                    Log.d("Gallery", e.getMessage());
                }

            }
        }
    }

//    public static Bitmap rotateImage(Bitmap source, float angle) {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle);
//        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
//                true);
//    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private Bitmap fixRotation(Uri data) {
        int exifRotation = CropUtil.getExifRotation(CropUtil.getFromMediaUri(
                this, this.getContentResolver(), data));
        Bitmap rotateBitmap = null;
        InputStream is = null;
        try {
            is = this.getContentResolver().openInputStream(data);
            BitmapFactory.Options option = new BitmapFactory.Options();
            Bitmap bmp = BitmapFactory.decodeStream(is, null, option);
            Matrix m = new Matrix();

            m.postRotate(exifRotation);
            rotateBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), m, true);
            // bmp.recycle();

        } catch (IOException e) {
        } catch (OutOfMemoryError e) {
        } finally {
            CropUtil.closeSilently(is);
        }
        return rotateBitmap;
    }

//    void doScanFile(String fileName) {
//        String[] filesToScan = {fileName};
//        MediaScannerConnection.scanFile(this, filesToScan, null,
//                new MediaScannerConnection.OnScanCompletedListener() {
//
//                    @Override
//                    public void onScanCompleted(String path, Uri uri) {
//                        mediaFileScanComplete(path, uri);
//                    }
//                });
//    }

//    void mediaFileScanComplete(String mediaFilePath, Uri mediaFileUri) {
//        lastMediaFilePath = mediaFilePath;
//        lastMediaFileUri = mediaFileUri;
//    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_CAMERA: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the task you need to do.
                    ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if ((ContextCompat.checkSelfPermission(FieldOfficerDetailActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                            ) {

                        requestPermissions(
                                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                        return;
                    } else {
                        selectImage();
                    }
                } else {

                    // permission denied, boo! Disable the functionality that
                    // depends on this permission.
                    Toast.makeText(
                            this,
                            "CAMERA Denied! This application might not function correctly.",
                            Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the task you need to do.
                    selectImage();
                } else {

                    // permission denied, boo! Disable the functionality that
                    // depends on this permission.
                    Toast.makeText(
                            this,
                            "WRITE_EXTERNAL_STORAGE Denied! This application might not function correctly.",
                            Toast.LENGTH_SHORT).show();
                }
                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
        }
    }

}
