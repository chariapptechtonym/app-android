package org.codearray.crecentrelife.MainHome.UserLogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.MyNetworks.NetworkMainActivity;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_CONTACTS;

public class UserLoginActivity extends AppCompatActivity implements View.OnClickListener {



    EditText _usernameInput;
    EditText _passwordInput;

    String  usernameInput , passwordInput;

    TextView _forgotPasswordBtn;
    ImageView _loginBtn;

    TextView _signUpBtn;

    LoginSession loginSession;
    private static final int PERMISSION_REQUEST_CODE = 200;

    private void initViews()
    {
        _usernameInput = (EditText) findViewById(R.id.email_input_id);
        _passwordInput = (EditText) findViewById(R.id.password_input_id);
        _signUpBtn = (TextView) findViewById(R.id.sign_up_btn);

        _forgotPasswordBtn = (TextView) findViewById(R.id.reset_pass_id);
        _loginBtn = (ImageView) findViewById(R.id.login_butn_id);

        loginSession = new LoginSession(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        //startup views
        initViews();

        //setup click events
        _loginBtn.setOnClickListener(this);
        _forgotPasswordBtn.setOnClickListener(this);
        _signUpBtn.setOnClickListener(this);


        if (ContextCompat.checkSelfPermission(UserLoginActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
           // launchMultiplePhonePicker();
        }else{
          //  Toast.makeText(UserLoginActivity.this, "Remember to go into settings and enable the contacts permission.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(UserLoginActivity.this
                    , new String[]{ACCESS_FINE_LOCATION , ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        //Snackbar.make(view, "Permission Granted, Now you can access Contacts.", Snackbar.LENGTH_LONG).show();
                        //launchMultiplePhonePicker();
                    } else {

                       // Snackbar.make(, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION , ACCESS_COARSE_LOCATION},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(UserLoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.login_butn_id:
                //if(!_validate()){
                //    return;
                //}else{
                //ToastMessage("Login Success");

                //you have to run the async task here once the api is done.
                //currently shifting to home screen directly.


                //}

                if(!_validate()){
                    return;
                }else{
                    new LoginTask().execute();
                }
                break;

            case R.id.reset_pass_id:
               // ToastMessage("Forgot Password Clicked");
                break;

            case R.id.sign_up_btn:
                Intent goReg = new Intent(UserLoginActivity.this, UserRegActivity.class);
                startActivity(goReg);
                finish();
                break;
        }

    }



    //show a tost message
    private  void ToastMessage( String Messge){
       // Toast.makeText(getApplicationContext(), Messge, Toast.LENGTH_LONG).show();
    }


    //validate data
    boolean _validate()
    {
        boolean valid = true;
        // _name = name.getText().toString();
        usernameInput = _usernameInput.getText().toString();
        passwordInput = _passwordInput.getText().toString();

        if (usernameInput.isEmpty()) {
            _usernameInput.setError("please provide your email");
            valid = false;
        } else {
            _usernameInput.setError(null);
        }


        if (passwordInput.isEmpty()) {
            _passwordInput.setError("please provide your password");
            valid = false;
        } else {
            _passwordInput.setError(null);
        }

        return valid;
    }



    //login task goes here.
    public class LoginTask extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(UserLoginActivity.this)
                    .title("Please Wait")
                    .content("Logging you in")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.UserLogin(usernameInput, passwordInput);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();

            try
            {
                if(s.contains("forbidden"))
                {
                    MaterialDialog.Builder dialog = new MaterialDialog.Builder(UserLoginActivity.this)
                            .title("Attention..!!")
                            .content("You have provided incorrect Login details or your account might not be active. Please check your email in case you have registered a new account")
                            .positiveText("Login");
                    dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            //// TODO
                        }
                    });
                    dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                else {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");
                    // AppConsts.ShowToast(getApplicationContext() , String.valueOf(getData));
                    for (int i = 0; i < getData.length(); i++) {
                        JSONObject c = getData.getJSONObject(i);
                        String user_id = c.getString("user_id");
                        String user_ref_id = c.getString("user_ref_id");
                        String user_first_name = c.getString("user_first_name");
                        String user_last_name = c.getString("user_last_name");
                        String user_mobile = c.getString("user_mobile");
                        String country = c.getString("user_add_cont");
                        String state = c.getString("user_add_state");
                        String city = c.getString("user_add_city");
                        String street = c.getString("user_add_st");
                        String ou_id = c.getString("ou_id");
                        String user_role = c.getString("role_name");
                        Log.d("userdetais  ", user_id + "-------" + user_first_name + "-----" + ou_id);
                        // AppConsts.ShowToast(getApplicationContext() , user_id + "-------" + ou_id + "-------" + user_first_name + "-------" + user_last_name + "-------" + user_mobile+ "-------" + country + "-------" + state + "-------" + city + "-------" + street+ "-------" + usernameInput+ "-------" + user_role );
                        loginSession.createLoginSession(user_id, ou_id, user_first_name, user_last_name, user_mobile, country, state, city, street, usernameInput, user_role, user_ref_id);
                    }
                    Intent goHome = new Intent(UserLoginActivity.this, MainActivity.class);
                    startActivity(goHome);
                    finish();
                }

            }catch (Exception e)
            {

            }





        }
    }
}
