package org.codearray.crecentrelife.MainHome.UserHome.HomeFrags;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;




import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.R;


import android.*;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class MasjidFrag extends Fragment implements LocationListener {



    boolean lastTime = false;
    private Button start;
    private LocationManager locMan;
    private Marker userMarker;
    private boolean updateFinished = true;
    private Marker[] markers;
    private final int MAX_PLACES = 20;
    private MarkerOptions[] markerOptions;
    int fragNum;

    MapView mMapView;
    private GoogleMap googleMap;

    public static MasjidFrag init(int val) {
        MasjidFrag truitonList = new MasjidFrag();
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragNum = getArguments() != null ? getArguments().getInt("val") : 1;

    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_masjid,
                container, false);

        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,1);
        locMan = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mMapView = (MapView) layoutView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                markers = new Marker[MAX_PLACES];
                enableGPS();
            }
        });


        return layoutView;
    }
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
//            Toast.makeText(getActivity(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    void enableGPS() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

            } else
                checkIfGPSIsOn(locMan, getActivity());
        } else {
            locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 10, this);
           // updatePlaces(getLastKnownLocation());
            new GetUserMapLocation().execute();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted


                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 10, this);

                    new GetUserMapLocation().execute();

                } else {

                    // permission denied
                    Toast.makeText(getActivity(), "GPS Disabled, enable GPS and allow app to use the service. Or app will not work properly.", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    protected void checkIfGPSIsOn(LocationManager manager, Context context) {
        if (!manager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Location Manager");
            builder.setMessage("Masjid Locator requires a device with GPS to work.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //finish();
                }
            });
            builder.create().show();
        } else {

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //Ask the user to enable GPS
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Location Manager");
                builder.setMessage("Masjid Locator would like to enable your device's GPS?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Launch settings, allowing user to make a change
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //No location service, no Activity
                        //finish();
                    }
                });
                builder.create().show();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (updateFinished && getActivity() != null)
            updatePlaces(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    void getPermissionLastTime()
    {
        if(!lastTime) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}, 1);

                } else
                    checkIfGPSIsOn(locMan, getActivity());
            } else {
                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 10, this);
                //updatePlaces(getLastKnownLocation());
                new GetUserMapLocation().execute();
            }
        }

        lastTime=true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @SuppressWarnings({"MissingPermission"})
    public class GetUserMapLocation  extends AsyncTask<Void, Void, Location> {

        @Override
        protected Location doInBackground(Void... voids) {
            locMan = (LocationManager) getActivity().getApplicationContext().getSystemService(LOCATION_SERVICE);
            List<String> providers = locMan.getProviders(true);
            Location bestLocation = null;
            for (String provider : providers) {
                Location l = locMan.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            return bestLocation;
        }

    @Override
    protected void onPostExecute(Location location) {
        super.onPostExecute(location);
        updatePlaces(location);

    }
}


    @SuppressWarnings({"MissingPermission"})
    private void updatePlaces(Location lastLoc) {
        //get location manager
        //get last location
        double lat=0 ;
        double lng=0;
        if(lastLoc!=null) {
            lat = lastLoc.getLatitude();
            lng = lastLoc.getLongitude();
        }
        else
        {
            if(getActivity() == null)
            {
                return;
            }
            locMan= (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            lastLoc=locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(lastLoc!=null) {
                lat = lastLoc.getLatitude();
                lng = lastLoc.getLongitude();
            }
            else
            {

                getPermissionLastTime();


            }
        }


        if(lastLoc !=null ) {
           // frameLayout.setEnabled(true);

            //create LatLng
            LatLng lastLatLng = new LatLng(lat, lng);

            Geocoder geocoder = new Geocoder(this.getActivity().getApplicationContext(), Locale.getDefault());

            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(lat, lng, 1);


            } catch (IOException e) {
                e.printStackTrace();
            }
            //remove any existing marker

            if (addresses != null) {
                if (addresses.size() > 0) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("user_add_st", addresses.get(0).getSubLocality());
                    editor.putString("user_add_city", addresses.get(0).getLocality());
                    editor.putString("user_add_cont", addresses.get(0).getCountryName());
                    editor.putString("user_add_state", addresses.get(0).getAdminArea());

                    editor.commit();
                }
            }
            if (userMarker != null)
                userMarker.remove();
            //create and set marker properties
            userMarker = googleMap.addMarker(new MarkerOptions()
                    .position(lastLatLng)
                    .title("You are here")
                    .snippet("Your last recorded location"));
            //move to location
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 15));


            String placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                    "json?location=" + lat + "," + lng +
                    "&radius=15000&sensor=true" +
                    "&types=mosque" +
                    "&key=AIzaSyDHk9P6AEVGd-1mGPkwLUHW-tiZHDuOjic";


                new GetPlaces().execute(placesSearchStr);
           // }
//
        }
        else
        {

            Toast.makeText(getActivity(), "Location could not be found, please enable location and allow app to use location and internet.", Toast.LENGTH_LONG).show();
        }
    }

    private class GetPlaces extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... placesURL) {
            //fetch markerOptions
            updateFinished = false;
            StringBuilder placesBuilder = new StringBuilder();
            for (String placeSearchURL : placesURL) {
                try {

                    URL requestUrl = new URL(placeSearchURL);
                    HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
                    connection.setRequestMethod("GET");
                    connection.connect();
                    int responseCode = connection.getResponseCode();

                    if (responseCode == HttpURLConnection.HTTP_OK) {

                        BufferedReader reader = null;

                        InputStream inputStream = connection.getInputStream();
                        if (inputStream == null) {
                            return "";
                        }
                        reader = new BufferedReader(new InputStreamReader(inputStream));

                        String line;
                        while ((line = reader.readLine()) != null) {

                            placesBuilder.append(line + "\n");
                        }

                        if (placesBuilder.length() == 0) {
                            return "";
                        }

                        Log.d("test", placesBuilder.toString());
                    } else {
                        Log.i("test", "Unsuccessful HTTP Response Code: " + responseCode);
                        return "";
                    }
                } catch (MalformedURLException e) {
                    Log.e("test", "Error processing Places API URL", e);
                    return "";
                } catch (IOException e) {
                    Log.e("test", "Error connecting to Places API", e);
                    return "";
                }
            }
            return placesBuilder.toString();
        }
        protected void onPostExecute(String result) {
            if (markers != null) {
                for (int pm = 0; pm < markers.length; pm++) {
                    if (markers[pm] != null)
                        markers[pm].remove();
                }
            }
            try {
                JSONObject resultObject = new JSONObject(result);
                JSONArray placesArray = resultObject.getJSONArray("results");
                markerOptions = new MarkerOptions[placesArray.length()];
                for (int p = 0; p < placesArray.length(); p++) {
                    boolean missingValue = false;
                    LatLng placeLL = null;
                    String placeName = "";
                    String vicinity = "";

                    try {

                        missingValue = false;
                        JSONObject placeObject = placesArray.getJSONObject(p);
                        JSONObject loc = placeObject.getJSONObject("geometry")
                                .getJSONObject("location");
                        placeLL = new LatLng(Double.valueOf(loc.getString("lat")),
                                Double.valueOf(loc.getString("lng")));
                        JSONArray types = placeObject.getJSONArray("types");
                        for (int t = 0; t < types.length(); t++) {
                            String thisType = types.get(t).toString();
                            if (thisType.contains("mosque")) {
                                break;
                            } else if (thisType.contains("health")) {
                                break;
                            } else if (thisType.contains("doctor")) {
                                break;
                            }
                        }
                        vicinity = placeObject.getString("vicinity");
                        placeName = placeObject.getString("name");
                    } catch (JSONException jse) {
                        Log.v("PLACES", "missing value");
                       // Toast.makeText(MainActivity.this,"Could not fetch data from server", Toast.LENGTH_LONG).show();
                        missingValue = true;
                        jse.printStackTrace();
                    }
                    if (missingValue) markerOptions[p] = null;
                    else
                        markerOptions[p] = new MarkerOptions()
                                .position(placeLL)
                                .title(placeName)
                                .snippet(vicinity).icon(BitmapDescriptorFactory.fromResource(R.drawable.mosq));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (markerOptions != null && markers != null) {


                for (int p = 0; p < markerOptions.length && p < markers.length; p++) {
                    //will be null if a value was missing

                    if (markerOptions[p] != null) {

                        markers[p] = googleMap.addMarker(markerOptions[p]);

                    }
                }


            }

            updateFinished=true;
        }


    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onResume() {
        super.onResume();
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,1);
        if (googleMap != null) {
            locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, this);
        }
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onPause() {
        super.onPause();
        if (googleMap != null) {
            locMan.removeUpdates(this);
        }
    }
    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onDestroy() {
        super.onDestroy();
        locMan.removeUpdates(this);
    }


}
