package org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Types;

/**
 * Created by Haseeb on 4/4/18.
 */

public enum BaseTimeAdjustmentType {
    TWO_MINUTES_ADJUSTMENT,
    TWO_MINUTES_ZUHR;
}
