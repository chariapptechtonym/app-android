package org.codearray.crecentrelife.MainHome.UserHome.Benifits;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;

import org.codearray.crecentrelife.R;

import java.text.SimpleDateFormat;

/**
 * Created by Haseeb on 4/9/18.
 */

public class BenifitsAdapter extends ArrayAdapter<String> {


    String[] offer_name;
    String[] org_id;
    String[] voucher_end_date;
    String[] voucher_location;
    String[] voucher_conditions;
    String[] voucher_discount;
    String[] voucher_id;
    String[] ou_id;
    private final Activity context;


    private MaterialDialog nDialog;

    Thread thread ;
    public final static int QRcodeWidth = 500 ;
    Bitmap bitmap ;

    public BenifitsAdapter(Activity context, String[] offer_name, String[] voucher_end_date, String[] voucher_conditions, String[] voucher_discount, String[] voucher_id , String[] org_id , String[] voucher_location, String[] ou_id) {
        super(context, R.layout.benifits_view, offer_name);
        this.context = context;
        this.offer_name = offer_name;
        this.voucher_end_date = voucher_end_date;
        this.voucher_conditions = voucher_conditions;
        this.voucher_discount = voucher_discount;
        this.voucher_id = voucher_id;
        this.org_id = org_id;
        this.voucher_location = voucher_location;
        this.ou_id = ou_id;
    }

    public class LoadViews
    {
        TextView offername;
        TextView enddate;
        TextView address;
        TextView condtions;
        TextView discount;
        ImageView qrcodeView;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LoadViews viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.benifits_view, null);
            viewHolder = new LoadViews();
            viewHolder.offername = (TextView) convertView.findViewById(R.id.offer_name);
            viewHolder.condtions = (TextView) convertView.findViewById(R.id.conditions);
            viewHolder.discount = (TextView) convertView.findViewById(R.id.total_discount);
            viewHolder.enddate = (TextView) convertView.findViewById(R.id.valid_upto);
            viewHolder.address = (TextView) convertView.findViewById(R.id.address);
            viewHolder.qrcodeView = (ImageView) convertView.findViewById(R.id.qrcodeView);
            //viewHolder.btn = (Button) convertView.findViewById(R.id.explorerButton);
            convertView.setTag(viewHolder);
        }else
        {
            viewHolder = (LoadViews) convertView.getTag();
        }



        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy:MM:dd");

        viewHolder.offername.setText(offer_name[position]);
        viewHolder.condtions.setText(voucher_conditions[position]);
        viewHolder.discount.setText(voucher_discount[position]+"");
        viewHolder.enddate.setText(voucher_end_date[position]);
        viewHolder.address.setText(voucher_location[position]);

        String imageURL = "https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl="+voucher_id[position]+","+org_id[position]+","+ou_id[position]+","+voucher_end_date[position]+","+voucher_discount[position]+","+voucher_location[position]+"&choe=UTF-8&chld=H%7C1";
        Picasso.get()
                .load(imageURL)
                .resize(50, 50)
                .centerCrop()
                .into(viewHolder.qrcodeView);
//        try {
//            bitmap = TextToImageEncode(voucher_id[position]);
//
//            viewHolder.qrcodeView.setImageBitmap(bitmap);
//
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }
//        //viewHolder.offername.setText(offer_name[position]);

        return  convertView;
    }


    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                         context.getResources().getColor(R.color.colorTitle):context.getResources().getColor(R.color.colorwhite);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);

        return bitmap;
    }

}
