package org.codearray.crecentrelife.MainHome;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Haseeb on 4/1/18.
 */

public class AppConsts {

    public static void ShowToast(Context context , String message)
    {
        Toast.makeText(context , message , Toast.LENGTH_LONG).show();
    }
}
