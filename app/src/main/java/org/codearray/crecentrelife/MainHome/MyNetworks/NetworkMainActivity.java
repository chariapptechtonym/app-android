package org.codearray.crecentrelife.MainHome.MyNetworks;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.*;
import android.provider.Contacts;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.MultiContactPicker;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.BenifitsAdapter;
import org.codearray.crecentrelife.MainHome.UserHome.Benifits.UserBenifitsActivity;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

public class NetworkMainActivity extends AppCompatActivity implements View.OnClickListener {



    //buttons
    ImageView addNetwork;
    TextView goBackHomeNets;
    SharedPreferences prefs;
    ListView networklistview;
    JSONArray getData;
    LinearLayout showView;


    LoginSession loginSession;
    String userId;

    private static final int CONTACT_PICKER_REQUEST = 991;
    private boolean mPermissionDenied = false;
    private View view;
    List<ContactResult> results;

    TextView textViewTitle;

    public static final int REQUEST_CODE_PICK_CONTACT = 1;
    public static final int  MAX_PICK_CONTACT= 10;

    private static final int PERMISSION_REQUEST_CODE = 200;
    String feed;



    String[] phone;
    String[] email;
    String[] firstname;

    private void initViews()
    {
        addNetwork = (ImageView) findViewById(R.id.network_add_btn_id);
        goBackHomeNets = (TextView) findViewById(R.id.gobackNets);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        showView = (LinearLayout) findViewById(R.id.no_friend_list);

        networklistview = (ListView) findViewById(R.id.network_list_view);
    }


    private void launchMultiplePhonePicker() {
        new MultiContactPicker.Builder(NetworkMainActivity.this) //Activity/fragment context
                // .theme(R.style.MyCustomPickerTheme) //Optional - default: MultiContactPicker.Azure
                .hideScrollbar(false) //Optional - default: false
                .showTrack(true) //Optional - default: true
                .searchIconColor(Color.WHITE) //Optional - default: White
                .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
                .handleColor(ContextCompat.getColor(NetworkMainActivity.this, R.color.colorPrimary)) //Optional - default: Azure Blue
                .bubbleColor(ContextCompat.getColor(NetworkMainActivity.this, R.color.colorPrimary)) //Optional - default: Azure Blue
                .bubbleTextColor(Color.WHITE) //Optional - default: White
                .showPickerForResult(CONTACT_PICKER_REQUEST);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_main);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        initViews();
        addNetwork.setOnClickListener(this);
        goBackHomeNets.setOnClickListener(this);

        loginSession = new LoginSession(this);  //making class

        HashMap<String ,String> detail = loginSession.getUserDetails();
        userId =detail.get(LoginSession.ouid);
        userId = prefs.getString("ou_id","");

        showView.setVisibility(View.INVISIBLE);
        new LoadBenifits().execute();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.network_add_btn_id:
                //Intent closeNet = new Intent(NetworkMainActivity.this, AddMember.class);
               // startActivity(closeNet);
               // finish();
                view = v;
                if (ContextCompat.checkSelfPermission(NetworkMainActivity.this, READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    launchMultiplePhonePicker();
                }else{
                //    Toast.makeText(NetworkMainActivity.this, "Remember to go into settings and enable the contacts permission.", Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(NetworkMainActivity.this
                            , new String[]{READ_CONTACTS}, PERMISSION_REQUEST_CODE);
                }
                break;

            case R.id.gobackNets:
                Intent backtoGhr = new Intent(NetworkMainActivity.this, MainActivity.class);
                startActivity(backtoGhr);
                finish();
                break;

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    // boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        Snackbar.make(view, "Permission Granted, Now you can access Contacts.", Snackbar.LENGTH_LONG).show();
                        launchMultiplePhonePicker();
                    } else {

                        Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{READ_CONTACTS},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(NetworkMainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONTACT_PICKER_REQUEST){
            if(resultCode == RESULT_OK) {
                results = MultiContactPicker.obtainResult(data);
             //   Toast.makeText(getApplicationContext() , results.get(0).getDisplayName(), Toast.LENGTH_LONG).show();
                Log.d("MyTag", results.get(0).getDisplayName());
                feed = new Gson().toJson(results);
                Snackbar.make(view , feed, Snackbar.LENGTH_LONG).show();
//                new NetworkAddSession().execute();
                try {
                    if(checkAlreadyExist(results) == false) {
                        new NetworkAddSession().execute();
                    }
                    else
                    {
                        MaterialDialog.Builder dialog = new MaterialDialog.Builder(NetworkMainActivity.this)
                                .title("Invitations Already sent...!!")
                                .content("You cannot invite twice")
                                .positiveText("OK");
                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(getIntent());
                                overridePendingTransition(0, 0);
                                //// TODO

                            }
                        });
                        dialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if(resultCode == RESULT_CANCELED){
                System.out.println("User closed the picker without selecting items.");
            }
        }
    }

    public boolean checkAlreadyExist(List<ContactResult> check) throws JSONException {
        for (int i=0; i<getData.length() ; i++)
        {
            try {
                JSONObject c = getData.getJSONObject(i);

                if(checkEmailExist(check, c.getString("network_email")) || checkPhoneExist(check,c.getString("network_phone")))
                {
                  return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return false;
    }


    public boolean checkPhoneExist(List<ContactResult> check,String phoneNumber)
    {
        for(int i = 0;i<check.get(0).getPhoneNumbers().size();i++)
        {
            String phoneNum = check.get(0).getPhoneNumbers().get(i).toString();
            if(check.get(0).getPhoneNumbers().get(i).toString().equals(phoneNumber))
            {
                return true;
            }
        }
        return false;
    }
    public boolean checkEmailExist(List<ContactResult> check,String email)
    {
        for(int i = 0;i<check.get(0).getEmails().size();i++)
        {
            if(check.get(0).getEmails().get(i).toString().equals(email))
            {
                return true;
            }
        }
        return false;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent closeNet = new Intent(NetworkMainActivity.this, MainActivity.class);
        startActivity(closeNet);
        finish();
    }

    public class NetworkAddSession extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(NetworkMainActivity.this)
                    .title("Please Wait")
                    .content("Submitting your Requests")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.AddNetwork(userId, feed);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();




            MaterialDialog.Builder dialog = new MaterialDialog.Builder(NetworkMainActivity.this)
                    .title("Invitations Sent...!!")
                    .content("Your Invitations have been sent succesfully")
                    .positiveText("OK");
            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog dialog, DialogAction which) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                    //// TODO

                }
            });
            dialog.show();





        }
    }


    //load contact....!!
    public class LoadBenifits extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(NetworkMainActivity.this)
                    .content("Getting your Contacts...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.GetNetwork("1", userId);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();

            if(s == null)
            {
                return;
            }
            if(s.contains("access"))
            {
//                new MaterialDialog.Builder(NetworkMainActivity.this)
//                        .content("No Groups Found")
//                        .positiveText("Ok")
//                        .show();

                showView.setVisibility(View.VISIBLE);
                textViewTitle.setVisibility(View.INVISIBLE);
            }
            else {
                //  Const.showToast(AppStart.this, s);
                try {
                    JSONObject jsonObj = new JSONObject(s);
                    getData = jsonObj.getJSONArray("result");



                    phone = new String[getData.length()];
                    email = new String[getData.length()];
                    firstname = new String[getData.length()];

                    if (getData.length() > 0) {

                        for (int i = 0; i < getData.length(); i++) {
                            JSONObject c = getData.getJSONObject(i);
                            phone[i] = c.getString("network_phone");
                            email[i] = c.getString("network_email");
                            firstname[i] = c.getString("network_firstname");
                        }


                        NetworkAdptater adapter = new NetworkAdptater(NetworkMainActivity.this , phone, email, firstname);
                        networklistview.setAdapter(adapter);
                        // mainCatListView.setAdapter(adapter);
                        //CategoriesMainAdpater adapter = new CategoriesMainAdpater(getActivity() , cat_id, usernsame, cat_name, cat_desc, cat_image );
                        //mainCatListView.setAdapter(adapter);

                    }else{
                        AppConsts.ShowToast(getApplicationContext() , "No Contacts");
                    }
                } catch (Exception e) {

                }
            }

        }
    }
}
