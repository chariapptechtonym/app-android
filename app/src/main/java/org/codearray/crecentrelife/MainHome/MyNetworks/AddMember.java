package org.codearray.crecentrelife.MainHome.MyNetworks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.R;

public class AddMember extends AppCompatActivity implements View.OnClickListener {

    EditText mail;
    EditText name;
    EditText phone;
    Button saveBtn;

    String _mail , _name, _phone;


    private void initViews()
    {
        mail = (EditText) findViewById(R.id.net_person_email);
        name = (EditText) findViewById(R.id.net_person_name);
        phone = (EditText) findViewById(R.id.net_person_phone);
        saveBtn = (Button) findViewById(R.id.net_save_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        initViews();
        saveBtn.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent closeNet = new Intent(AddMember.this, NetworkMainActivity.class);
        startActivity(closeNet);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.net_save_btn:
                if(!_validate()){
                    return;
                }else{

                    MaterialDialog.Builder dialog = new MaterialDialog.Builder(this)
                            .title("Invite Send")
                            .content("An Invitation has been sent to the person")
                            .positiveText("OK");
                    dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            //// TODO
                          //  AppConsts.ShowToast(getApplicationContext() , "Done");
                            Intent closeNet = new Intent(AddMember.this, NetworkMainActivity.class);
                            startActivity(closeNet);
                            finish();
                        }
                    });
                    dialog.show();
                }
                break;
        }

    }


    boolean _validate()
    {
        boolean valid = true;
        // _name = name.getText().toString();
        _mail = mail.getText().toString();
        _name = name.getText().toString();
        _phone = phone.getText().toString();

        if (_name.isEmpty()) {
            name.setError("Please provide the name for Member");
            valid = false;
        } else {
            name.setError(null);
        }


        if (_mail.isEmpty() || _phone.isEmpty()) {
            mail.setError("Please provide an Email");
            phone.setError("Please provide Phone");
            valid = false;
        } else {
            mail.setError(null);
            phone.setError(null);
        }

        return valid;
    }
}
