package org.codearray.crecentrelife.MainHome.UserHome.Family;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.codearray.crecentrelife.R;


public class FamilyDetail extends AppCompatActivity {
    ImageView imgProfile;
    TextView txtGender,txtAge,txtCity,txtStatus,txtAct,txtActSecond,txtName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_detail);
        imgProfile = findViewById(R.id.recp_profile_pic);
        txtGender = findViewById(R.id.gender_type_recp);
        txtAge = findViewById(R.id.age_recp);
        txtCity = findViewById(R.id.city_recp);
        txtStatus = findViewById(R.id.recp_status);
        txtAct = findViewById(R.id.act_recp);
        txtActSecond = findViewById(R.id.actSecond_recp);
        txtName = findViewById(R.id.lblName);
        Intent i  = getIntent();
        String age = i.getStringExtra("age");
        txtGender.setText(i.getStringExtra("gender"));
        txtAge.setText(i.getStringExtra("age"));
        txtCity.setText(i.getStringExtra("city"));
        txtStatus.setText(i.getStringExtra("status"));
        txtAct.setText(i.getStringExtra("act"));
        txtActSecond.setText(i.getStringExtra("actSecond"));
        txtName.setText(i.getStringExtra("fullName"));
    }

}
