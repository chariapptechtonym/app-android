package org.codearray.crecentrelife.MainHome.UserHome.Family;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class RecipientUpdates extends AppCompatActivity {



    TextView username;
    TextView gender;
    TextView age;
    TextView city;
    TextView status;
    ImageView userPhoto;

    AlertDialog dialogBuilder;

    ListView activitesListView;

    ImageView updateBtn;

    String userrole;
    LoginSession loginSession;

    String _name , _gender , _agr , _city, _status,  _id;
    private static final int PICK_PHOTO_FOR_AVATAR = 0;

    private static final int PERMISSION_REQUEST_CODE = 200;


    //get recp details here...
    String[] recp_act_detail;
    String[] recp_act_link;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipient_updates);


        prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        username = (TextView) findViewById(R.id.recpname);
        userPhoto = (ImageView) findViewById(R.id.recp_profile_pic);
        updateBtn = (ImageView) findViewById(R.id.edit_update_btn_recp);
        gender = (TextView) findViewById(R.id.gender_type_recp);
        age = (TextView) findViewById(R.id.age_recp);
        city = (TextView) findViewById(R.id.city_recp);
        status = (TextView) findViewById(R.id.recp_status);
        activitesListView = (ListView) findViewById(R.id.updates_list_view);

        Intent getData = getIntent();
        _name = getData.getStringExtra("name");
        _gender = getData.getStringExtra("gender");
        _agr = getData.getStringExtra("age");
        _city = getData.getStringExtra("city");
        _status = getData.getStringExtra("status");
        _id = getData.getStringExtra("id");

        username.setText(_name);
        gender.setText(_gender);
        age.setText(_agr);
        city.setText(_city);
        status.setText(_status);


        loginSession = new LoginSession(this);  //making class

        //HashMap<String ,String> detail = loginSession.getUserDetails();
        userrole =prefs.getString("role_name","");//detail.get(LoginSession.user_role);


        if(userrole.equalsIgnoreCase("User") || userrole.equalsIgnoreCase("Sponser")){
            updateBtn.setVisibility(View.INVISIBLE);
        }


        if (ContextCompat.checkSelfPermission(RecipientUpdates.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // launchMultiplePhonePicker();
        }else{
            //  Toast.makeText(UserLoginActivity.this, "Remember to go into settings and enable the contacts permission.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(RecipientUpdates.this
                    , new String[]{READ_EXTERNAL_STORAGE , WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }

        new FamilyList().execute();

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calibrateDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted) {
                        //Snackbar.make(view, "Permission Granted, Now you can access Contacts.", Snackbar.LENGTH_LONG).show();
                        //launchMultiplePhonePicker();
                    } else {

                        // Snackbar.make(, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{READ_EXTERNAL_STORAGE , WRITE_EXTERNAL_STORAGE},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(RecipientUpdates.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    String updateText = "";
    private void calibrateDialog(){
        LayoutInflater layoutInflater = LayoutInflater.from(RecipientUpdates.this);
        View calibrateView = layoutInflater.inflate(R.layout.recipent_update_dialog, null);

        Button btn = (Button) calibrateView.findViewById(R.id.pic_vid_btn);
        final EditText userupdate = (EditText) calibrateView.findViewById(R.id.user_updates);


        dialogBuilder = new AlertDialog.Builder(RecipientUpdates.this)
                .setView(calibrateView)
                .setTitle("Recipient Updates")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        dialogBuilder.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = ((AlertDialog) dialogBuilder).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        //Dismiss once everything is OK.
                        updateText = userupdate.getText().toString();
                        if(isFileChosen) {
                            new UploadUpdates().execute();
                        }
                        else{
                            AppConsts.ShowToast(getApplicationContext() , "Please select an Image File against Recipient Details");
                        }
                       // dialogBuilder.dismiss();
                    }
                });

                Button button1  = ((AlertDialog) dialogBuilder).getButton(AlertDialog.BUTTON_NEGATIVE);
                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogBuilder.dismiss();
                    }
                });
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        dialogBuilder.show();

    }

    public void pickImage() {
       // Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
      //  intent.setType("image/*");
      //  startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_PHOTO_FOR_AVATAR);
    }


    String filePath;
    String fileType;
    File imgFile;
    int filesize;
    boolean isFileChosen = false;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                Uri imageUri;
                imageUri = data.getData();
                fileType = getContentResolver().getType(imageUri);

//                System.out.println("------------------------------------");
//                System.out.println(String.valueOf(imageUri));
//                System.out.println(String.valueOf(inputStream.toString()));
//                System.out.println(getContentResolver().getType(imageUri));
//                System.out.println("------------------------------------");

//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
//                Uri tempUri = getImageUri(getApplicationContext(), photo);
//
//                // CALL THIS METHOD TO GET THE ACTUAL PATH
//                File finalFile = new File(getRealPathFromURI(tempUri));
//                System.out.println("------------------------------------");
//                System.out.println(String.valueOf(finalFile));
//                System.out.println(fileType);
//                System.out.println("------------------------------------");

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                filePath = cursor.getString(columnIndex);
                cursor.close();

                imgFile = new  File(filePath);
                filesize = Integer.parseInt(String.valueOf(imgFile.length()/1024));

                isFileChosen = true;
                System.out.println("------------------------------------");
                System.out.println(filePath);
                System.out.println(fileType);
                System.out.println(filesize);
                System.out.println("------------------------------------");




            } catch (Exception e) {
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    public class UploadUpdates extends AsyncTask<Void, Long, String> {
        private  ProgressDialog mDialog;
        private  MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(RecipientUpdates.this)
                    .title("Please Wait")
                    .content("Uploading Updates")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.UploadActivity(_id, updateText,filePath , fileType, imgFile );
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onProgressUpdate(Long... progress) {

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();

            MaterialDialog.Builder dialog = new MaterialDialog.Builder(RecipientUpdates.this)
                    .title("Updated")
                    .content("Recipient Activity has been updated successfully")
                    .positiveText("OK");
            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog dialog, DialogAction which) {
                    //// TODO

                }
            });
            dialog.show();

        }
    }


    //get user activites here....
    public class FamilyList extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(RecipientUpdates.this)
                    .content("Getting Activites...")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.GetRecpActivites(_id);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();


            if(!s.contains("result"))
            {
//                new MaterialDialog.Builder(FamilyActivity.this)
//                        .content("No Family Found")
//                        .positiveText("Ok")
//                        .show();
               // nofamily.setVisibility(View.VISIBLE);
            }
            else {
                //  Const.showToast(AppStart.this, s);
                try {
                    JSONObject jsonObj = new JSONObject(s);
                    JSONArray getData = jsonObj.getJSONArray("result");
                    recp_act_detail = new String[getData.length()];
                    recp_act_link = new String[getData.length()];

                    if (getData.length() > 0) {

                        for (int i = 0; i < getData.length(); i++) {
                            JSONObject c = getData.getJSONObject(i);
                            recp_act_detail[i] = c.getString("recp_act_detail");
                            recp_act_link[i] = c.getString("recp_act_link");

                        }

                        RecpActAdpater adapter = new RecpActAdpater(RecipientUpdates.this ,recp_act_detail, recp_act_link);
                        activitesListView.setAdapter(adapter);


                    }
                } catch (Exception e) {

                }
            }

        }
    }
}
