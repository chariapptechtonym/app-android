package org.codearray.crecentrelife.MainHome.NamazTimeLibaray;

/**
 * Created by Haseeb on 4/4/18.
 */

import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.KAABA_LATITUDE;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.KAABA_LONGITUDE;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Constants.TOTAL_DEGREES;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Util.MathUtil.atan2Deg;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Util.MathUtil.cosDeg;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Util.MathUtil.sinDeg;
import static org.codearray.crecentrelife.MainHome.NamazTimeLibaray.Util.MathUtil.tanDeg;

public class QiblaUtils {


    public static float qibla(float latitude, float longitude) {
        double degrees = atan2Deg(sinDeg(KAABA_LONGITUDE - longitude),
                cosDeg(latitude) * tanDeg(KAABA_LATITUDE)
                        - sinDeg(latitude) * cosDeg(KAABA_LONGITUDE - longitude));
        return (float) (degrees >= 0 ? degrees : degrees + TOTAL_DEGREES);
    }
}
