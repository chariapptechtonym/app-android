package org.codearray.crecentrelife.MainHome.UserHome.Family;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.codearray.crecentrelife.R;

/**
 * Created by Haseeb on 4/27/18.
 */

public class RecpActAdpater extends ArrayAdapter<String> {

    String[] recp_act_detail;
    String[] recp_act_link;

    private final Activity context;

    public RecpActAdpater(Activity context, String[] recp_act_detail, String[] recp_act_link) {
        super(context, R.layout.recp_act_view, recp_act_detail);
        this.context = context;
        this.recp_act_detail = recp_act_detail;
        this.recp_act_link = recp_act_link;

    }

    public class LoadViews
    {

        TextView userUpdate;
        ImageView userPhoto;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RecpActAdpater.LoadViews viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.recp_act_view, null);
            viewHolder = new LoadViews();
            viewHolder.userUpdate = (TextView) convertView.findViewById(R.id.user_update_act);
            viewHolder.userPhoto = (ImageView) convertView.findViewById(R.id.image_update_view);
            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (RecpActAdpater.LoadViews) convertView.getTag();
        }

        viewHolder.userUpdate.setText(recp_act_detail[position]);

        Picasso.get()
                .load("https://chariapp.com/api/app/Controllers/uploads/"+recp_act_link[position])
                .resize(50, 50)
                .into(viewHolder.userPhoto);
        return  convertView;
    }
}
