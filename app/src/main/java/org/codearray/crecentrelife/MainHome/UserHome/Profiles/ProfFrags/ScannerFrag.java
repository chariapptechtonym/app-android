package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;
import org.codearray.crecentrelife.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class ScannerFrag extends Fragment {



    ImageView btnScan;

   // LoginSession loginSession;
    String ouid,expiry,discount,location;
    String vid, orgid;



    int fragNum;
    public static ScannerFrag init(int val) {
        ScannerFrag truitonList = new ScannerFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        truitonList.setArguments(args);

        return truitonList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragNum = getArguments() != null ? getArguments().getInt("val") : 1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_scanner,
                container, false);

       // codeView = (TextView) layoutView.findViewById(R.id.);
        //loginSession = new LoginSession(getActivity());

        //HashMap<String ,String> detail = loginSession.getUserDetails();

       // ouid = detail.get(LoginSession.ouid);
        btnScan = (ImageView) layoutView.findViewById(R.id.scanner_btn);


        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startQRScanner();
            }
        });

        return layoutView;
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//    }

    private void startQRScanner() {
        //new IntentIntegrator(getActivity()).initiateScan();
        IntentIntegrator.forSupportFragment(this).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result =   IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(getActivity(),    "Cancelled",Toast.LENGTH_LONG).show();
            } else {
               //ssss updateText(result.getContents());
                Toast.makeText(getActivity(),    result.getContents(),Toast.LENGTH_LONG).show();

                String str = result.getContents();
                List<String> elephantList = Arrays.asList(str.split(","));
                System.out.println("------------------------");
                System.out.println(elephantList.get(0));
                System.out.println(elephantList.get(1));
                System.out.println(elephantList.get(2));
                vid = elephantList.get(0);
                orgid = elephantList.get(1);
                ouid = elephantList.get(2);
                expiry = elephantList.get(3);
                discount = elephantList.get(4);
                location = elephantList.get(5);
                System.out.println("------------------------");

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure u want to redeem this Voucher with id: "+vid +" \nDiscount : "+discount +"%\n"+
                "expiry : "+expiry)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                new RedeemVoucher().execute();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                // Create the AlertDialog object and return it
                builder.create();
                builder.show();

            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
            Toast.makeText(getActivity(),    result.getContents(),Toast.LENGTH_LONG).show();
        }
    }


    public class RedeemVoucher extends AsyncTask<Void, Void, String> {
        private MaterialDialog nDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog =  new MaterialDialog.Builder(getActivity())
                    .title("Please Wait")
                    .content("Verifying the Voucher Code")
                    .progress(true, 0)
                    .backgroundColor(Color.WHITE)
                    .contentColor(Color.BLACK)
                    .titleColor(Color.BLACK)
                    .dividerColor(Color.BLACK)
                    .widgetColor(Color.parseColor("#55B0CF"))
                    .show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString = null;
            try {
                responseString = HttpRequests.RedeemVoucher(vid , ouid , orgid);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(nDialog.isShowing())
                nDialog.dismiss();



            if(s.contains("granted")){
                MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity())
                        .title("Voucher Applied")
                        .content("Voucher has been Applied successfully and eligble for a Discount")
                        .positiveText("Ok");

                dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                });
                dialog.show();
            }
            else{
                MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity())
                        .title("OPPS...!!")
                        .content("Voucher seemed to expired or already consumed.")
                        .positiveText("Ok");

                dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                    }
                });
                dialog.show();
            }
        }
    }
}
