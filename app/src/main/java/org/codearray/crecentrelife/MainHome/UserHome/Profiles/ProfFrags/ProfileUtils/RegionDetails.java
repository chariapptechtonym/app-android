package org.codearray.crecentrelife.MainHome.UserHome.Profiles.ProfFrags.ProfileUtils;

/**
 * Created by Haseeb on 4/19/18.
 */

public class RegionDetails {

    private String regionName;
    private String regionCountCode;

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCountCode() {
        return regionCountCode;
    }

    public void setRegionCountCode(String regionCountCode) {
        this.regionCountCode = regionCountCode;
    }
}
