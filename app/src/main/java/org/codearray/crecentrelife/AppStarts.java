package org.codearray.crecentrelife;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.codearray.crecentrelife.MainHome.AppConsts;
import org.codearray.crecentrelife.MainHome.Requests.HttpRequests;
import org.codearray.crecentrelife.MainHome.UserHome.MainActivity;
import org.codearray.crecentrelife.MainHome.UserLogin.UserLoginActivity;
import org.codearray.crecentrelife.MainHome.UserRegistration.UserRegActivity;
import org.codearray.crecentrelife.MainHome.Utils.LoginSession;

import java.io.IOException;
import java.util.HashMap;

public class AppStarts extends AppCompatActivity {

    private static  int splash_time_out = 2000;

    LoginSession loginSession;  // to store the user session for the auto login.
    String userId;      //storeing the user id.
    public final static int REQUEST_CODE = 10101;

    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_app_starts);

        startService(new Intent(getBaseContext(), AbstractService.class));


        loginSession = new LoginSession(this);  //making class

        HashMap<String ,String> detail = loginSession.getUserDetails();
        userId =detail.get(LoginSession.userid);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

               // if (checkDrawOverlayPermission()) {
//                    if (userId != null) {
                        Intent i = new Intent(AppStarts.this, MainActivity.class);
                        startActivity(i);
//                    } else {
//                        Intent i = new Intent(AppStarts.this, UserLoginActivity.class);
//                        startActivity(i);
//                    }

               // }
               
                finish();

            }
        }, splash_time_out);


        prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat("previousTime", (float) 0.0);
        editor.commit();
    }

    private  String  checkApp() throws IOException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://testsite.devsmeme.com/")
                .get()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Postman-Token", "89ca5c2a-679e-4688-a48e-0dd32a48e6f3")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }


}
