package org.codearray.crecentrelife;

import android.net.Uri;

import java.io.File;

/**
 * Created by HP on 6/7/2018.
 */

public class CaptureImage  {
    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public void setFile(File file) {
        this.file = file;
    }

    Uri uri;

    public File getFile() {
        return file;
    }

    public Uri getUri() {
        return uri;
    }

    File file;
}